<?php
namespace App;

use Ratchet\ConnectionInterface;
use Ratchet\MessageComponentInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use App\Entity\User;
use App\Entity\Mensaje;
use Symfony\Component\HttpFoundation\JsonResponse;

class Notification implements MessageComponentInterface
{
    protected $connections = array();
    protected $sesionsEstadisticas =  array();
    protected $sesions =  array();
    protected $container;
    /**
     * @param ConnectionInterface $from
     * @param string $msg
     */
    public function onMessage(ConnectionInterface $from, $msg)
    {
        $messageData = json_decode(trim($msg));
        $user = $messageData->userData;
        $token_user = $messageData->token;
        if($this->validarToken($token_user, $user)){
          echo "Token válido\n";
            if($messageData->tipo==2){            //---------------CASO MENSAJE DE CHAT
              $destino = $messageData->destino;
              $sucursal = $messageData->sucursal;
              echo "para :"; echo $destino;echo "\n de:";echo $user;echo " \n";
              $em = $this->container->get('doctrine')->getManager();
              $repo = $em->getRepository('App:User');
              $nombreEmisor = $repo->findOneBy(array('id' => $user));
              $mensaje= json_encode(array('nombre' => $nombreEmisor->getUsername() ,'tipo' => $messageData->tipo, 'origen'=> $user, 'mensaje' =>$messageData->mensaje ));
              $all_connections = $this->connections;
              $sesion =$this->sesions[$destino];
              $sesionOrigen= $this->sesions[$user];
              echo "\n sesiones encontadas";
              if( $sesion == null){
                echo "------------------------------------------Usuario no conectado";
              }else{
                foreach($all_connections as $key => $conn){ //ENVIAR MENSAJE A TODOS LOS SOQUET DE TIPO CHATS QUE ESTAN EN LA MATRIZ
                    if(in_array($conn->resourceId, $sesion) ){
                      $conn->send($mensaje);    echo "------------------------------------$mensaje ---- $conn->resourceId  \n";
                    }else if(in_array($conn->resourceId, $sesionOrigen) and $conn!=$from ){
                        $conn->send($mensaje);  echo "------------------------------------$mensaje -- a origen -- $conn->resourceId  \n";
                    }else{ continue;
                    }
                }
              }
              echo "-----------------------------------------------------GUARDANDO EN BASE DE DATOS";
              $em = $this->container->get('doctrine')->getManager()->getConnection();
              $query = "INSERT INTO mensaje ( contenido, id_user_a_id, id_user_b_id, sucursal_id, fecha ) values ( :contenido, :idUserA, :idUserB, :sucursal, :fecha)";
              $stmt = $em->prepare($query);
              $params = array('contenido' => $mensaje,'idUserA' =>  $user,'idUserB' => $destino,'sucursal' => $sucursal,'fecha' => date("Y-m-d H:i:s") );
              $stmt->execute($params);

            }elseif($messageData->tipo==3){// CASO MENSAJE DE ESTADISTICAS
              $destino = //$messageData->destino;
              $sucursal = $messageData->sucursal;
              echo "\mensaje de estadisticas";
            }elseif($messageData->tipo==4){// CASO INICIO DE ESTADISTICAS
              echo "\n -------------------------------sesion estadisticas -------------:"; 
              $sucursal = $messageData->sucursal;
              $this->adminMatrizConection($sucursal, $from->resourceId, 1, "estadisticas"); // CREA O REGISTRA UNA NUEVA SESION EN LA MATRIS DE ESTADISTICA
              //formato: tipo:4 , producto..., id, sucursal
            }elseif($messageData->tipo==1){// CASO INICIO DE CHAT
              $this->adminMatrizConection($messageData->userData, $from->resourceId, 1, "chats"); //CREA O REGISTRA UNA NUEVA SESION EN LA MATRIZ DE CHATS
            }else{
              $from->send("ERROR: este tipo de mensaje no está contemplado\n");
            }
        }else {
          echo "Token inválido\n";
          $conn->close();
        }
    }

    /**
     * A connection is closed
     * @param ConnectionInterface $conn
     */
    public function onClose(ConnectionInterface $conn)
    {
      $socket=$conn->resourceId;
      $objetivo="";
      echo "\n---------------------------------------eliminando: -";
      echo $socket;
      try {
      $matriz= &$this->sesions;//Buscando en conecciones de chats para quitarla
        foreach($matriz as $key =>  $cons){
          if(array_search($socket, $cons)){
              $k=array_search($socket, $cons);
              unset($matriz[$key][$k]);
              break;
          }
        }
      $matriz= &$this->sesionsEstadisticas;//Buscando en conecciones estadisticas  para quitarla
        foreach($matriz as $key => $cons){
          if(array_search($socket, $cons)){
              $k=array_search($socket, $cons);
              unset($matriz[$key][$k]);
              break;
          }
        }
      }catch (Exception $e){
        echo 'Excepción capturada: ',  $e->getMessage(), "\n";
      }
      echo "\n--------------------------------------------------------------------------------------";
      echo '\n---------------------------------Sesiones usuario------------------';
      var_dump($this->sesions);
      echo '\n---------------------------------Sesiones estadistica--------------';
      var_dump($this->sesionsEstadisticas);
        foreach($this->connections as $key => $conn_element){
            if($conn === $conn_element){
                //$this->adminMatrizConection("desconocido", $conn->resourceId, -1, $objetivo);
                echo "close! ({$conn->resourceId})\n";
                unset($this->connections[$key]);
                break;
            }
        }
    }

    /**
     * Error handling
     *
     * @param ConnectionInterface $conn
     * @param \Exception $e
     */
    public function onError(ConnectionInterface $conn, \Exception $e)
    {
        $conn->send("Error : " . $e->getMessage());
        //$conn->close();
    }

        /**
     * @param int
     * @param string
     * @param integer
     * @param string
     */
    public function adminMatrizConection($user, $socket, $delete, $banderaMatriz){
      switch ($banderaMatriz) {
        case "chats":
            $matriz = & $this->sesions;
            break;
        case "estadisticas":
            $matriz = & $this->sesionsEstadisticas;
            break;
        case 2:

            break;
      }  
      if(array_key_exists($user, $matriz ) && $delete==1){
        echo "\nAgregando sesion"; 
        array_push($matriz[$user], $socket);
      }
      elseif($delete==1){
        echo '\nCreando sesion';
        $matriz[$user] = [$socket];
      }elseif($delete==-1){
        
        try {
          foreach($matriz as $key => $conn){
          if(array_search($socket, $conn))
              $k= array_search($socket, $conn );
              unset($matriz[$key][$k]);
          }
        } catch (Exception $e){
            echo 'Excepción capturada: ',  $e->getMessage(), "\n";
        }
      }
      echo '\n---------------------------------Sesiones usuario------------------';
      var_dump($this->sesions);
      echo '\n---------------------------------Sesiones Estadistica--------------';
      var_dump($this->sesionsEstadisticas);
    }

    

  public function __construct(ContainerInterface $container){
      $this->container = $container;
  }
  /**
   * A new websocket connection
   *
   * @param ConnectionInterface $conn
   */
  public function onOpen(ConnectionInterface $conn){
        $this->connections[] = $conn;
        $conn->send('conección de retorno');
        echo "New connection! ({$conn->resourceId})\n";
  }
  /**
   * @param string
   * @param string
   * @return boolean
   */
  protected function validarToken($token, $user){
    echo "-----------------------------validando\n token recibido vs token\n";
    $em = $this->container->get('doctrine')->getManager();
    $em->clear();
    $repo = $em->getRepository('App:User');
    $entidad = $repo->findOneBy(array('id' => $user));
    $tokenReal = $entidad->gettokenSocket();
    echo $token."\n\n";

    echo $tokenReal;
    echo "\n------------\n";
    //$em->remove($entidad);
      if($tokenReal == $token){
          return true;
      } else{
          return false;
      }
  }
}
