<?php

namespace App\Repository;

use App\Entity\UsuarioClientes;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method UsuarioClientes|null find($id, $lockMode = null, $lockVersion = null)
 * @method UsuarioClientes|null findOneBy(array $criteria, array $orderBy = null)
 * @method UsuarioClientes[]    findAll()
 * @method UsuarioClientes[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UsuarioClientesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UsuarioClientes::class);
    }

    // /**
    //  * @return UsuarioClientes[] Returns an array of UsuarioClientes objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?UsuarioClientes
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
