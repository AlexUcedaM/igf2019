<?php

namespace App\Repository;

use App\Entity\AdminSistema;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method AdminSistema|null find($id, $lockMode = null, $lockVersion = null)
 * @method AdminSistema|null findOneBy(array $criteria, array $orderBy = null)
 * @method AdminSistema[]    findAll()
 * @method AdminSistema[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AdminSistemaRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AdminSistema::class);
    }

    // /**
    //  * @return AdminSistema[] Returns an array of AdminSistema objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?AdminSistema
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
