<?php

namespace App\Controller;

use App\Entity\Menu;
use App\Form\MenuType;
use App\Repository\MenuRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * @Route("/menu")
 */
class MenuController extends AbstractController
{

  /**
  * @Route("/combo_menu_ajax", name="combo_menu_ajax", methods={"POST"})
  */
  public function combo_menu_ajax(Request $request){
    $sucursal = $request->get('sucursal');
    $em = $this->getDoctrine()->getManager();
    $query = $em->createQuery("SELECT c.id as ids, c.nombre as nombre FROM App:Menu c
      WHERE (c.idSucursal = :sucursal)"
    );
    $query->setParameter('sucursal',$sucursal);
    $salida = $query->getResult();
    json_encode($salida);
      $jsonArray = array(
        'data' => $salida,
        'success' => true,
    );
    return new JsonResponse($jsonArray);
  }

  /**
  * @Route("/eliminar_menu_ajax", name="eliminar_menu_ajax", methods={"POST"})
  */
  public function eliminar_menu_ajax(Request $request){
    $id = $request->get('menu');
    $entityManager = $this->getDoctrine()->getManager();
    $repository = $entityManager->getRepository('App:Menu');
    $menu = $repository->findOneBy(array('id' => $id));
    $entityManager->remove($menu);
    $entityManager->flush();

       $jsonArray = array(
           'success' => true,
       );
       return new JsonResponse($jsonArray);
  }

  /**
  * @Route("/editar_menu_ajax", name="editar_menu_ajax", methods={"POST"})
  */
  public function editar_menu_ajax(Request $request){
    $id = $request->get('menu');
    $nombre = $request->get('nombre');
    $descripcion = $request->get('descripcion');

    $entityManager = $this->getDoctrine()->getManager();
    $repository = $entityManager->getRepository('App:Menu');
    $menu = $repository->findOneBy(array('id' => $id));

    $menu->setnombre($nombre);
    $menu->setDescripcion($descripcion);
    $entityManager->persist($menu);
    $entityManager->flush();
       $jsonArray = array(
           'success' => true,
       );
       return new JsonResponse($jsonArray);
  }

  /**
  * @Route("/crear_menu_ajax", name="crear_menu_ajax", methods={"POST"})
  */
  public function crear_menu_ajax(Request $request){
    $sucursal = $request->get('sucursal');
    $nombre = $request->get('nombre');
    $descripcion = $request->get('descripcion');
    $entityManager = $this->getDoctrine()->getManager();
    $repository = $entityManager->getRepository('App:Sucursal');
    $sucursal = $repository->findOneBy(array('id' => $sucursal));

    $menu = new Menu();
    $menu->setnombre($nombre);
    $menu->setDescripcion($descripcion);
    $menu->setIdSucursal($sucursal);
    $entityManager->persist($menu);
    $entityManager->flush();


       $jsonArray = array(
           'success' => true,
       );
       return new JsonResponse($jsonArray);
  }

  /**
  * @Route("/verificarNombreMenu", name="verificarNombreMenu", methods={"POST"})
  */
  public function verificarNombreMenu(Request $request){
    $nombre = $request->get('nombre');
    $sucursal = $request->get('sucursal');

    $em = $this->getDoctrine()->getManager();
        $query = $em->createQuery("SELECT u.nombre as nombre FROM App\Entity\Menu u WHERE (u.nombre = :nombre and u.idSucursal= :sucursal)");
        $query->setParameters( array('nombre' => $nombre, 'sucursal' => $sucursal ));
        $entidad = $query->getResult();
        if($entidad == null){
            $jsonArray = array('data' => 'Bien', );
            return new JsonResponse($jsonArray);
        }
        else{
            $jsonArray = array('data' => 'repetido', );
            return new JsonResponse($jsonArray);
        }
  }



  //------------------------------------------------------------------------------------------------------------------------
    /**
     * @Route("/", name="menu_index", methods={"GET"})
     */
    public function index(MenuRepository $menuRepository): Response
    {
        return $this->render('menu/index.html.twig', [
            'menus' => $menuRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="menu_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $menu = new Menu();
        $form = $this->createForm(MenuType::class, $menu);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($menu);
            $entityManager->flush();

            return $this->redirectToRoute('menu_index');
        }

        return $this->render('menu/new.html.twig', [
            'menu' => $menu,
            'formMenu' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="menu_show", methods={"GET"})
     */
    public function show(Menu $menu): Response
    {
        return $this->render('menu/show.html.twig', [
            'menu' => $menu,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="menu_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Menu $menu): Response
    {
        $form = $this->createForm(MenuType::class, $menu);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            return $this->redirectToRoute('menu_index');
        }

        return $this->render('menu/edit.html.twig', [
            'menu' => $menu,
            'formMenu' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="menu_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Menu $menu): Response
    {
        if ($this->isCsrfTokenValid('delete'.$menu->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($menu);
            $entityManager->flush();
        }

        return $this->redirectToRoute('menu_index');
    }
}
