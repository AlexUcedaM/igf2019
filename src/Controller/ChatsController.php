<?php

namespace App\Controller;

//use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
//MENEJO DE RESPUESTAS HTML, JSON
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Entity\User;
use App\Entity\Mensaje;
use App\Entity\Pedido;
use App\Repository\ProductoRepository;
use App\Repository\SucursalRepository;
//MANEJOD DE SERVICIOS
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
//-------prueba
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactory;
use Symfony\Component\Serializer\Mapping\Loader\AnnotationLoader;
use Doctrine\Common\Annotations\AnnotationReader;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\PropertyInfo\Extractor\ReflectionExtractor;
//
class ChatsController extends FOSRestController
{

    /**
   * @Rest\POST("api/listaChats/")
   *
  */
  public function servicioListaMensajes(Request $request, SerializerInterface $serializer )
  {
    $id = (int) $this->get('security.token_storage')->getToken()->getUser()->getUsername();
    $sucursal = (int)$request->get('sucursal');
    $entityManager = $this->getDoctrine()->getManager();
    $query = $entityManager->createQuery('
    SELECT  s
      FROM App\Entity\Sucursal s
        LEFT JOIN App\Entity\Mensaje m
        WITH s.id = m.sucursal
        WHERE (m.idUserA = :id or m.idUserB = :id)
        order by m.id'
      )->setParameters(array('id' => $id ));
      $md = $query->getResult();

      $classMetadataFactory = new ClassMetadataFactory(new AnnotationLoader(new AnnotationReader()));
      $normalizer = new ObjectNormalizer($classMetadataFactory);
      $serializer = new Serializer([new DateTimeNormalizer(),$normalizer]);

      $data = $serializer->normalize($md, 'json', ['groups' => 'listaChats']);
      //$data = $serializer->normalize($md, 'json', ['groups' => 'GMensajesUsuario']);
      //-----------RESPUESTA-------------------------------
      $response = new Response();
      $response->setContent(
        json_encode($data)
      );
      return $response;
  }

  /**
   * @Rest\POST("api/chats/")
   *
  */
    public function ServicioMensajes(Request $request, SerializerInterface $serializer )
  {
    $id = (int) $this->get('security.token_storage')->getToken()->getUser()->getUsername();
    $sucursal = (int)$request->get('sucursal');
    $entityManager = $this->getDoctrine()->getManager();
    $query = $entityManager->createQuery('
    SELECT  m
      FROM App\Entity\Mensaje m
        INNER JOIN App\Entity\User u
        WITH m.idUserA = u.id or m.idUserB = u.id
        WHERE ((m.idUserA = :id or m.idUserB = :id) AND m.sucursal= :sucursal)
        order by m.id'
        )->setParameters(array('id' => $id , 'sucursal' => $sucursal ));
      $md = $query->getResult();

      $classMetadataFactory = new ClassMetadataFactory(new AnnotationLoader(new AnnotationReader()));
      $normalizer = new ObjectNormalizer($classMetadataFactory);
      $serializer = new Serializer([new DateTimeNormalizer(),$normalizer]);

      $data = $serializer->normalize($md, 'json', ['groups' => 'GmensajeInvert']);
      //$data = $serializer->normalize($md, 'json', ['groups' => 'GMensajesUsuario']);
      //-----------RESPUESTA-------------------------------
      $response = new Response();
      $response->setContent(
        json_encode($data)
      );
      return $response;
  }

  /**
  * @Route("/prueba/{sucursal}", name="prodsasas", methods={"GET","POST"})
  */
  public function pruebaAction(Request $request, SucursalRepository $SucursalRepository, SerializerInterface $serializer)
  {
    $sucursals = $SucursalRepository->findAll();
    $sucursal = (int)$request->get('sucursal');
    $em = $this->container->get('doctrine')->getManager()->getConnection();
      $query =
        "SELECT * FROM (
            SELECT I.nombre as Menu, J.* FROM (
                SELECT H.nombre, H.id FROM (SELECT E.id FROM public.sucursal AS E WHERE E.id =:sucursal) AS U
                LEFT JOIN menu AS H
                ON U.id = H.id_sucursal_id
            ) AS I
                LEFT JOIN producto  J
                ON J.menu_id= I.id
          ) AS X ORDER BY X.menu
        ";
      $stmt = $em->prepare($query);
      $stmt->execute(array('sucursal' => $sucursal));
      $menus= $stmt->fetchAll();
      $cadena="";
      foreach ($menus as $key => $value) {
        foreach($value as $tag) {
              $cadena= $cadena . "--" . $tag;
        }
      }
      foreach ($menus as $project){
        $cadena= $cadena . "--" . $project['menu'];
      }

      $id = (int)$request->get('id');
      $sucursal= (int)$request->get('sucursal');
      $entityManager = $this->getDoctrine()->getManager();
      $query = $entityManager->createQuery('
        SELECT  m
          FROM App\Entity\Mensaje m
            INNER JOIN App\Entity\User u
            WITH m.idUserA = u.id or m.idUserB = u.id
                LEFT JOIN App\Entity\Sucursal s
                WITH m.sucursal = s.id
            WHERE s.id = :sucursal AND (m.idUserA = :id or m.idUserB = :id)
            order by m.id'
      )->setParameters(array('id' => 14, 'sucursal' =>2 ));
    $md = $query->getResult();
    $mdA = $query->getArrayResult();

    //$classMetadataFactory = new ClassMetadataFactory(new AnnotationLoader(new AnnotationReader()));
    //$normalizer = new ObjectNormalizer($classMetadataFactory);
    //$serializer = new Serializer([$normalizer]);

    $data = $serializer->normalize($md, 'json', ['groups' => 'Gmensaje', 'GUser']);

    $classMetadataFactory = new ClassMetadataFactory(new AnnotationLoader(new AnnotationReader()));
    $normalizer = new ObjectNormalizer($classMetadataFactory);
    $serializer = new Serializer([new DateTimeNormalizer(),$normalizer]);
    //-----------RESPUESTA-------------------------------
    $response = new Response();
    $response->setContent(json_encode($data));
    //------------------------------------
    $query = $entityManager->createQuery('
      SELECT  m FROM App\Entity\UsuarioClientes m where m.id=1');
    $md = $query->getResult();

    return $this->render('imprimirJsn.html.twig', array('json'=> json_encode($md1), 'result'=> $mdA,    'sucursals' => $sucursals, 'keys'=> array_keys($menus[0]), 'modelos'=>$response ));
}

/**
 * @Route("/render_chats_admins", name="render_chats_admins", methods={"POST"})
*/
    public function render_chats_admins(ProductoRepository $productoRepository, SucursalRepository $SucursalRepository, Request $request): Response
    {
      $sucursal = $request->get('sucursal');
      $user=$this->getUser()->getId();
      $em = $this->container->get('doctrine')->getManager()->getConnection();
      $query = "SELECT  I.emisor, J.username, I.sucursal FROM (
                  SELECT DISTINCT U.chat, U.emisor, U.sucursal FROM(
                    SELECT CONCAT(CAST(Y.emisor AS VARCHAR),CAST(Y.receptor AS VARCHAR)) AS chat, * FROM (
                      SELECT * ,
                      CASE T.id_user_b_id
                        WHEN :id0 THEN T.id_user_a_id
                        ELSE T.id_user_b_id
                        END AS emisor
                      FROM (
                      SELECT m.id, m.id_user_a_id, m.id_user_b_id, :id1 as receptor, m.sucursal_id as sucursal
                        FROM public.mensaje as m
                        WHERE ((m.id_user_a_id= :id2 OR m.id_user_b_id=:id3) AND m.id_user_b_id!=m.id_user_a_id AND m.sucursal_id= :sucursal)) AS T) AS Y) AS U) AS I
                  LEFT JOIN fos_user J
                  ON I.emisor= J.id
        ";
      $stmt = $em->prepare($query);
      $stmt->execute(array('id0' => $user,'id1' => $user, 'id2' => $user, 'id3' => $user,'sucursal' => $sucursal));
      $chats= $stmt->fetchAll();
      json_encode($chats);
      $jsonArray = array(
            'chats' => $chats,
            'success' => true,
            'sucursal' => $sucursal,
        );
      return $this->render('mensajes/mensajes.html.twig', ['data'=>$jsonArray ]);
    }

    /**
     * @Route("/chats/{sucursal}", name="mensajes", methods={"GET"})
    */
    public function mensajes(Request $request): Response
    {
        $sucursal = $request->get('sucursal');
        $user=$this->getUser()->getId();
        $em = $this->container->get('doctrine')->getManager()->getConnection();

      $query = "select  id, id_user_a_id, id_user_b_id, fecha, sucursal_id from  mensaje
      where (
            (id_user_a_id= :idUserA and  id_user_b_id= :idUserB)  or
            (id_user_a_id= :idUserB and  id_user_b_id= :idUserA )
      )";
      $stmt = $em->prepare($query);
      $stmt->execute(array('idUserA' =>  14,'idUserB' => 13,));
      $mensajes= $stmt->fetchAll();
      json_encode($mensajes);

      $query = "SELECT  I.emisor, J.username, I.sucursal FROM (
                	SELECT DISTINCT U.chat, U.emisor, U.sucursal FROM(
                		SELECT CONCAT(CAST(Y.emisor AS VARCHAR),CAST(Y.receptor AS VARCHAR)) AS chat, * FROM (
                			SELECT * ,
                			CASE T.id_user_b_id
                				WHEN :id0 THEN T.id_user_a_id
                				ELSE T.id_user_b_id
                			  END AS emisor
                			FROM (
              				SELECT m.id, m.id_user_a_id, m.id_user_b_id, :id1 as receptor, m.sucursal_id as sucursal
              					FROM public.mensaje as m
              					WHERE ((m.id_user_a_id= :id2 OR m.id_user_b_id=:id3) AND m.id_user_b_id!=m.id_user_a_id AND m.sucursal_id= :sucursal)) AS T) AS Y) AS U) AS I
                  LEFT JOIN fos_user J
                  ON I.emisor= J.id
        ";
      $stmt = $em->prepare($query);
      $stmt->execute(array('id0' => $user,'id1' => $user, 'id2' => $user, 'id3' => $user,'sucursal' => $sucursal));
      $chats= $stmt->fetchAll();
      json_encode($chats);
        $jsonArray = array(
              'chats' => $chats,
              'mensajes' => $mensajes,
              'success' => true,
              'sucursal' => $sucursal,
          );

        return $this->render('mensajes/mensajes.html.twig', ['data'=>$jsonArray ]);

    }

    /**
    * @Route("/pintarMensajes", name="pintarMensajes", methods={"POST"})
    */
    public function verMensajesAjax(Request $request){
        $sucursal = $request->get('sucursal');
        $user=$this->getUser()->getId();
        $emisor = $request->get('emisor');

        $em = $this->getDoctrine()->getManager();
        $query = $em->createQuery(
        "SELECT p
        FROM App:Mensaje p
        WHERE ( ((p.idUserA= :AU AND p.idUserB= :BU) OR (p.idUserA  = :BU AND p.idUserB= :AU)) AND (p.idUserB!=p.idUserA) AND (p.sucursal= :sucursal) )
        ORDER BY p.id asc"
        );
        $mensajes = $query->setParameters( array('AU' =>$user,'BU' =>$emisor, 'sucursal' =>$sucursal,))->getResult();


         return $this->render('mensajes/pintarMensajes.html.twig', [
             'mensajes' => $mensajes, 'user'=>$user,
         ]);
    }
}
