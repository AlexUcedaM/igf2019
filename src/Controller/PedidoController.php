<?php

namespace App\Controller;

use App\Entity\Pedido;
use App\Entity\Producto;
use App\Entity\Menu;
use App\Form\PedidoType;
use App\Repository\PedidoRepository;
use App\Repository\EstadoRepository;
use App\Repository\SucursalRepository;
use App\Repository\ProductoRepository;
//use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Form\ProductoType;
use App\Form\MenuType;

use App\Entity\PedidoProducto;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\Validator\Constraints\DateTime;

use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactory;
use Symfony\Component\Serializer\Mapping\Loader\AnnotationLoader;
use Doctrine\Common\Annotations\AnnotationReader;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\PropertyInfo\Extractor\ReflectionExtractor;
/**
 * @Route("/pedido")
 */
class PedidoController extends FOSRestController
{

  /**
   * @Route("/render_pedido_ajax", name="render_pedido_ajax", methods={"POST"})
   */
  public function render_pedido_ajax(PedidoRepository $pedidoRepository , EstadoRepository $EstadoRepository, Request $request): Response
  {
      $id = intval($request->get('sucursal'));
      $em = $this->getDoctrine()->getManager();
      $query = $em->createQuery("SELECT u FROM App\Entity\Pedido u WHERE u.sucursal= :id ");
      $query->setParameters( array('id' => $id ));
      $pedidos = $query->getResult();

      return $this->render('pedido/index.html.twig', [
          'pedidos' => $pedidos,
          'estados' => $EstadoRepository->findAll(),
      ]);
  }


  /**
  * @Route("/filtrar_pedidos_ajax", name="filtrar_pedidos_ajax", methods={"POST"})
  */
  public function filtrar_pedidos_ajax(Request $request){
      $id = intval($request->get('id'));
      $em = $this->getDoctrine()->getManager();
      $query = $em->createQuery("SELECT u FROM App\Entity\Pedido u WHERE u.sucursal= :id ");
      $query->setParameters( array('id' => $id ));
      $pedido = $query->getResult();
       return $this->render('pedido/_formTablaPedidos.html.twig', [
           'pedidos' => $pedido,
       ]);
  }

  /**
  * @Route("/cambiar_estado_ajax", name="cambiar_estado_ajax", methods={"POST"})
  */
  public function cambiar_estado_ajax(Request $request){
      $pedido = $request->get('pedido');
      $estado = $request->get('estado');

      $em = $this->getDoctrine()->getManager();
      $query = $em->createQuery(
      "update
       App\Entity\Pedido u  SET u.estado= :estado WHERE   u.id = :pedido ");
      $query->setParameters(array('pedido' => $pedido,'estado' => $estado  ));

    try {
        $productos = $query->getResult();
        $jsonArray = array('success' => 'true');
    } catch (DBALException $e) {
        $message = sprintf('DBALException [%i]: %s', $e->getCode(), $e->getMessage());
        $jsonArray = array('success' => 'dbal', 'error' => $message);
    } catch (PDOException $e) {
        $message = sprintf('PDOException [%i]: %s', $e->getCode(), $e->getMessage());
        $jsonArray = array('success' => 'PDO', 'error' => $message);
    } catch (ORMException $e) {
        $message = sprintf('ORMException [%i]: %s', $e->getCode(), $e->getMessage());
        $jsonArray = array('success' => 'ORM', 'error' => $message);
    } catch (Exception $e) {
        $message = sprintf('Exception [%i]: %s', $e->getCode(), $e->getMessage());
        $jsonArray = array('success' => 'Excepcion', 'error' => $message);
    }
    return new JsonResponse($jsonArray);
  }

    /**
    * @Route("/cargar_platillos_ajax", name="cargar_platillos_ajax", methods={"POST"})
    */
    public function cargar_platillos_ajax(Request $request){
        $pedido = $request->get('pedido');

        $em = $this->getDoctrine()->getManager();
        $query = $em->createQuery(
        "SELECT u.id, u.nombreProducto, u.precioVenta, i.cantidad, i.subTotal
        FROM App\Entity\Producto u
        LEFT JOIN App\Entity\PedidoProducto i WITH u.id = i.producto
         WHERE   i.pedido = :pedido ");
        $query->setParameters( array('pedido' => $pedido ));
        $productos = $query->getResult();

         return $this->render('producto/indexLista.html.twig', [
             'productos' => $productos,
         ]);
    }

    /**
     * @Route("/", name="pedido_index", methods={"GET"})
     */
    public function index(PedidoRepository $pedidoRepository , EstadoRepository $EstadoRepository, SucursalRepository $SucursalRepository): Response
    {
        return $this->render('pedido/index.html.twig', [
            'sucursals' => $SucursalRepository->findAll(),
            'pedidos' => $pedidoRepository->findAll(),
            'estados' => $EstadoRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="pedido_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $pedido = new Pedido();
        $form = $this->createForm(PedidoType::class, $pedido);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($pedido);
            $entityManager->flush();

            return $this->redirectToRoute('pedido_index');
        }

        return $this->render('pedido/new.html.twig', [
            'pedido' => $pedido,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="pedido_show", methods={"GET"})
     */
    public function show(Pedido $pedido): Response
    {
        return $this->render('pedido/show.html.twig', [
            'pedido' => $pedido,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="pedido_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Pedido $pedido): Response
    {
        $form = $this->createForm(PedidoType::class, $pedido);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('pedido_index');
        }

        return $this->render('pedido/edit.html.twig', [
            'pedido' => $pedido,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="pedido_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Pedido $pedido): Response
    {
        if ($this->isCsrfTokenValid('delete'.$pedido->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($pedido);
            $entityManager->flush();
        }

        return $this->redirectToRoute('pedido_index');
    }


      ///**
  // * @Rest\Get("/pedirDetallePedido/{id}", name= "detallesDePedido")
  // *
  //*/
  /*  public function ServicioMensajes(Request $request, SerializerInterface $serializer )
  {

    $F1= (int) $this->get('security.token_storage')->getToken()->getUser()->getUsername();
    $id = (int)$request->get('id');
    $entityManager = $this->getDoctrine()->getManager();
    $query = $entityManager->createQuery('
    SELECT  m FROM App\Entity\Pedido m where m.id =:id')->setParameters(array('id' => $id ));
      $md = $query->getResult();

      $classMetadataFactory = new ClassMetadataFactory(new AnnotationLoader(new AnnotationReader()));
      $normalizer = new ObjectNormalizer($classMetadataFactory);
      $serializer = new Serializer([new DateTimeNormalizer(),$normalizer]);

      $data = $serializer->normalize($md, 'json', ['groups' => 'productosPedido']);
      //-----------RESPUESTA-------------------------------
      $response = new Response();
      $response->setContent(json_encode($data));
      return $response;
  }
*/
// /* /**
// * Creates an Article resource
// * @Rest\Post("/enviarPedidos/")
// * @param Request $request
// * @return json
//*/
/*  
public function enviarPedidos(Request $request, SucursalRepository $SucursalRepository)
  {
    //$data=$request->getContent();
    //$sucursals = $SucursalRepository->findAll();
    $em = $this->getDoctrine()->getManager();
    $objPedido= new Pedido();
    $fecha = new \DateTime();
    $response = new Response();

    $cliente = $request->get('cliente');
    $sucursal = $request->get('sucursal');
    $productos = $request->get('productos');
    $r= $response->setContent( json_encode($productos));
    return $r;
    $ObjCliente=  $em->getRepository('App\Entity\UsuarioClientes')->findOneBy(array('id' => $cliente));
    $ObjSucursal= $em->getRepository('App\Entity\Sucursal')       ->findOneBy(array('id' => $sucursal ));
    $ObjEstado=   $em->getRepository('App\Entity\Estado')         ->findOneBy(array('id' => 1 ));

    $repoProductos = $em->getRepository('App\Entity\Producto');

    $objPedido->setFecha($fecha);
    $objPedido->setCliente($ObjCliente);
    $objPedido->setSucursal($ObjSucursal);
    $objPedido->setEstado($ObjEstado);
    $total=0;
    
    foreach($productos as $pedido){
      $objPedidoProducto= new PedidoProducto();
      $p=$repoProductos->findOneBy(array('id' => $pedido["producto"] ));
      $objPedidoProducto->setProducto($p);
      $objPedidoProducto->setCantidad( $pedido["cantidad"] );
      $subTotal= $objPedidoProducto->getCantidad()*$p->getPrecioVenta();
      $objPedidoProducto->setSubTotal($subTotal);
      $objPedido->addProductos($objPedidoProducto);
      $total= $total+($subTotal);
    }
    $objPedido->setTotal($total);
    $em->persist($objPedido);
    $em->flush();
    $array= array('total' => $total ,'idPedido' => $objPedido->getId() );

    //$normalizer = new ObjectNormalizer(null, null, null, new ReflectionExtractor());
    //$serializer = new Serializer([new DateTimeNormalizer(), $normalizer]);
    //$data=json_decode($data);
    //$data = $serializer->denormalize( $data , 'App\Entity\Pedido' );
    //$entityManager = $this->getDoctrine()->getManager();
    //$entityManager->persist($data);
    //$entityManager->flush();
    
    //if(empty($name) || empty($role)){
      $response->setContent( json_encode($array));
    //}
    return $response;
  } 
  */
}
