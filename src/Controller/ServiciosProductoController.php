<?php

namespace App\Controller;

use App\Entity\Producto;
use App\Entity\Menu;
use App\Form\ProductoType;
use App\Form\MenuType;
use App\Repository\ProductoRepository;
use App\Repository\MenuRepository;
use App\Repository\SucursalRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Doctrine\DBAL\DBALException;
use Doctrine\ORM\ORMException;
use Exception;
//---------------------SERVICIOS------------------------------
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;

use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactory;
use Symfony\Component\Serializer\Mapping\Loader\AnnotationLoader;
use Doctrine\Common\Annotations\AnnotationReader;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class ServiciosProductoController extends AbstractController
{

    /**
     * @Rest\Get("api/productos_menus/{sucursal}")
     *
     */
    public function productosMenus(Request $request)
    {
        $sucursal = (int)$request->get('sucursal');
        $entityManager = $this->getDoctrine()->getManager();
        $query = $entityManager->createQuery('
        SELECT m FROM App\Entity\Menu m
                INNER JOIN App\Entity\Sucursal s
                WITH s.id = m.idSucursal
            WHERE s.id = :sucursal order by m.id'
            )->setParameters(array('sucursal' => $sucursal,));
        $md = $query->getResult();

        $classMetadataFactory = new ClassMetadataFactory(new AnnotationLoader(new AnnotationReader()));
        $normalizer = new ObjectNormalizer($classMetadataFactory);
        $serializer = new Serializer([new DateTimeNormalizer(),$normalizer]);

        $data = $serializer->normalize($md, 'json', ['groups' => 'MenusProductos']);
        //$data = $serializer->normalize($md, 'json', ['groups' => 'GMensajesUsuario']);
        //-----------RESPUESTA-------------------------------
        $response = new Response();
        $response->setContent(json_encode($data));
        return $response;
    }
    
}
