<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Doctrine\DBAL\DBALException;
use Doctrine\ORM\ORMException;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Exception;
use FOS\UserBundle\Model\UserManagerInterface;
use App\Entity\UsuarioClientes;

class ApiLoginController extends FOSRestController
{
    private $userManager;

    public function __construct(UserManagerInterface $userManager)
    {
        $this->userManager = $userManager;
    }

    /**
    * @Rest\Post("api/getId/")
    * @param Request $request
    * @return Json
    */
    public function getIdUsuario(Request $request)
    {
        $idUsuario =  (int) $this->get('security.token_storage')->getToken()->getUser()->getUsername();
        $array= array('id' => $idUsuario);
        $response = new Response();
        $response->setContent(json_encode($array));
        return $response;
    }


    /**
    * @Rest\Post("api/perfil/")
    * @param Request $request
    * @return Json
    */
    public function perfilApi(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $idUsuario =  (int) $this->get('security.token_storage')->getToken()->getUser()->getUsername();
        //----------------------------------------------------------
        $user = $em->getRepository('App\Entity\User')->findOneById( $idUsuario );

        //--------------
        $array= array('email' => $user->getEmail() , 'username' => $user->getUsername(), 'direccion' =>  $user->getDireccion());
        $response = new Response();
        $response->setContent(json_encode($array));
        return $response;
    }

    /**
    * @Rest\Post("api/editRegistroApi/")
    * @param Request $request
    * @return Json
    */
    public function editRegistroApi(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $email= $request->get('email');
        $username = $request->get('username');
        $password= $request->get('password');
        $direccion= $request->get('direccion');
        $idUsuario =  (int) $this->get('security.token_storage')->getToken()->getUser()->getUsername();
        //----------------------------------------------------------
        $user = $em->getRepository('App\Entity\User')->findOneById( $idUsuario );
        try {
        if(!is_null($email)){
            $user->setEmail($email);
        }
        if(!is_null($username)){
            $user->setUsername($username);
            $user->setUsernameCanonical($username);
        }
        if(!is_null($password)){
            $user->setPlainPassword($password);
        }
        if(!is_null($direccion)){
            $user->setDireccion($direccion);
        }
        
            $this->userManager->updateUser($user);
            $em->flush();
        } catch (DBALException $e) {
            $jsonArray = array('estado' => 'false', 'error' =>'usuario o email ya estan registrados' );
            return new JsonResponse($jsonArray);
        }catch (UniqueConstraintViolationException $e) {
            $jsonArray = array('estado' => 'false', 'error' =>'usuario o email ya estan registrados' );
            return new JsonResponse($jsonArray);
        }
        //return $response;
        //--------------
        $array= array('estado' => 'true');
        $response = new Response();
        $response->setContent(json_encode($array));
        return $response;
    }
    

    /**
    * @Rest\Post("registro_api/")
    * @param Request $request
    * @return Json
    */
    public function registroApi(Request $request)
    {
        $email= $request->get('email');
        $username = $request->get('username');
        $password= $request->get('password');
        $direccion= $request->get('direccion');

        //----------------------------------------------------------
        try {
        $user = $this->userManager->createUser();
        $user->setEnabled(true);
        $user->setUsername($username);
        $user->setUsernameCanonical($username);
        $user->setEmail($email);
        $user->setEmailCanonical($email);
        $user->setPlainPassword($password);
        $user->setDireccion($direccion);
        $user->addRole('ROLE_CLIENTE');
        $this->userManager->updateUser($user);
            $usuario = new UsuarioClientes();
            $usuario->setUsuario($user);
            $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($usuario);
                $entityManager->flush();
                $jsonArray = array('estado' => 'true' );
                return new JsonResponse($jsonArray);
            }catch (UniqueConstraintViolationException $e) {
                $jsonArray = array('estado' => 'false', 'error' =>'usuario o email ya estan registrados' );
                return new JsonResponse($jsonArray);
            }catch (PDOException $e) {
                $jsonArray = array('estado' => 'false' , 'error' => 'es posibles que el email o usuario este registrado' );
                return new JsonResponse($jsonArray);
            } catch (ORMException $e) {
                $jsonArray = array('estado' => 'false' , 'error' => 'problema ORM' );
                return new JsonResponse($jsonArray);
            }catch (Exception $e) {
                $jsonArray = array('estado' => 'false' , 'error' => 'es posibles que el email o usuario este registrado' );
                return new JsonResponse($jsonArray);
            }
            
            $user->setUsuario($usuario);
        //return $response;
        //--------------
        $array= array('email' => $email ,'username' => $username, 'password' => $password ,'direccion' => $direccion);
        $response = new Response();
        $response->setContent( json_encode($array));

        return $response;
    }

    /**
    * @Rest\Post("api/login_api/")
    * @param Request $request
    * @return Json
    */
    public function loginApi(Request $request)
    {
        //$email= $request->get('email');
        //$username = $request->get('username');
        //$password= $request->get('password');
        //----------------------------------------------------------
        
        $array= array('login' => "true");
        $response = new Response();
        $response->setContent( json_encode($array));

        return $response;
    }

    /**
    * @Rest\Post("api/log/appLogout/", name="app_logout" )
    * @param Request $request
    * @return Json
    */
    public function AppLogout(Request $request)
    {
        $array= array('logout' => "true");
        $response = new Response();
        $response->setContent( json_encode($array));
        return $response;
    }

   
    /**
     * @Route("/log/appLogout/", name="appLogout", methods={"GET"})
     */
    public function appLogoutConfirm(Request $request)
    {
        $array= array('logout' => "true");
        $response = new Response();
        $response->setContent( json_encode($array));
        return $response;
    }

    /**
    * @Rest\Post("api/log/log/",  name="app")
    * @param Request $request
    * @return Json
    */
    public function log(Request $request)
    {
        //$email= $request->get('email');
        //$username = $request->get('username');
        //$password= $request->get('password');
        $array= array('sesion' => "abierta", 'usuario'=> (int) $this->get('security.token_storage')->getToken()->getUser()->getUsername() );
        $response = new Response();
        $response->setContent( json_encode($array));
        return $response;
    }

}
