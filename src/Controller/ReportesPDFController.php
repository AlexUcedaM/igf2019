<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Knp\Bundle\SnappyBundle\Snappy\Response\PdfResponse;
use Symfony\Component\HttpFoundation\Response;
use App\Repository\SucursalRepository;
use App\Repository\AdminSucursalRepository;
use Symfony\Component\HttpFoundation\Request;

class ReportesPDFController extends AbstractController
{

    /**
     * @Route("/reporteIndex/", name="render_reportes")
    */
    public function reporteIndex(SucursalRepository $SucursalRepository)
    {
        $sucursals = $SucursalRepository->findAll();
            
        return $this->render('reportesPdf/index.html.twig', [
            'sucursals' => $sucursals,
        ]);
    }

    /**
     * @Route("/reportePDF/{sucursal}/{fechaInicial}/{fechaFinal}/", name= "reportePDF")
    */
    public function reportePDF(Request $request,AdminSucursalRepository $AdminSucursalRepository, \Knp\Snappy\Pdf $snappy )
    {
        $idSucursal= $request->get('sucursal');
        $FI = (new \DateTime($request->get('fechaInicial')))->format( 'Y-m-d' ); // '2019-12-01'; 
        $FF = (new \DateTime($request->get('fechaFinal')))->format( 'Y-m-d' ) ;  // '2019-12-31';
        $idUser= $this->getUser()->getId();

        $admin=$AdminSucursalRepository->findOneByUsuario($idUser);
        $idAdmin=$admin->getId();

        $em = $this->getDoctrine()->getManager();
        $db= $em->getConnection();
        //----------------------------------------------------------------------------------------------------------
        //SELECT id, pedido_id, producto_id, cantidad, sub_total FROM public.pedido_producto
        
        $sql= //numero de ventas/costo venta / precio venta / menu/ agrupadas por producto para sucursal X en periodo i - f 
        "SELECT  pr.nombre_producto as Producto, m.nombre as Menu,  SUM(dp.cantidad) as Cantidad, sum(dp.sub_total) as Ingresos
            FROM public.pedido_producto dp
                LEFT JOIN public.pedido p
                ON dp.pedido_id=p.id
                LEFT JOIN public.producto pr
                ON pr.id=dp.producto_id
                LEFT JOIN public.menu m
                ON m.id=pr.menu_id
                where p.sucursal_id  = :sucursal and p.estado_id=4 and p.fecha<= :FF and p.fecha>= :FI group by pr.id, m.nombre"; //agrupado producto una sucursal
            $consulta= $db->prepare($sql);
            $consulta->execute(array( 'sucursal' => $idSucursal, 'FF' => $FF, 'FI' => $FI ));//$params
            $resultProductos = $consulta->fetchAll();

            $sql= //numero de ventas// precio venta / menu/ agrupadas por menus para sucursal X en periodo i - f 
            "SELECT  m.nombre as Menu, SUM(dp.cantidad) as Cantidad, sum(dp.sub_total) as Ingresos
                FROM public.pedido_producto dp
                    LEFT JOIN public.pedido p
                    ON dp.pedido_id=p.id
                    LEFT JOIN public.producto pr
                    ON pr.id=dp.producto_id
                    LEFT JOIN public.menu m
                    ON m.id=pr.menu_id
                    where p.sucursal_id  = :sucursal and p.estado_id=4 and p.fecha<= :FF and p.fecha>= :FI group by m.id"; //agrupado menu una sucursal
                $consulta= $db->prepare($sql);
                $consulta->execute(array( 'sucursal' => $idSucursal, 'FF' => $FF, 'FI' => $FI ));//$params
                $resultMenu = $consulta->fetchAll();

            $sql= //numero de ventas canceladas// precio venta / menu/ agrupadas por productos para sucursal X en periodo i - f 
            "SELECT pr.nombre_producto as Producto , m.nombre as Menu, SUM(dp.cantidad) as Cantidad, sum(dp.sub_total) as Ingresos
            FROM public.pedido_producto dp
                LEFT JOIN public.pedido p
                ON dp.pedido_id=p.id
                LEFT JOIN public.producto pr
                ON pr.id=dp.producto_id
                LEFT JOIN public.menu m
                ON m.id=pr.menu_id
                where p.sucursal_id  = :sucursal and p.estado_id=5 and p.fecha<= :FF and p.fecha>= :FI group by pr.id, m.nombre"; //agrupados producto una sucursal
                $consulta= $db->prepare($sql);
                $consulta->execute(array( 'sucursal' => $idSucursal, 'FF' => $FF, 'FI' => $FI ));//$params
                $resultProductosCancelados = $consulta->fetchAll();

            $sql= //numero de ventas// precio venta / menu/ agrupadas por menus para sucursal X en periodo i - f 
            "SELECT  m.nombre as Menu, SUM(dp.cantidad) as Cantidad, sum(dp.sub_total) as Ingresos
                FROM public.pedido_producto dp
                    LEFT JOIN public.pedido p
                    ON dp.pedido_id=p.id
                    LEFT JOIN public.producto pr
                    ON pr.id=dp.producto_id
                    LEFT JOIN public.menu m
                    ON m.id=pr.menu_id
                    where p.sucursal_id  = :sucursal and p.estado_id=5 and p.fecha<= :FF and p.fecha>= :FI group by m.id"; // agrupados por menu una sucursal
                $consulta= $db->prepare($sql);
                $consulta->execute(array( 'sucursal' => $idSucursal, 'FF' => $FF, 'FI' => $FI ));//$params
                $resultMenusCancelados = $consulta->fetchAll();
//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
            $sql= //numero de ventas/costo venta / precio venta / menu/ agrupadas por producto todas las sucursales en periodo i - f 
            "SELECT s.nombre as Sucursal, m.nombre as Menu, pr.nombre_producto as Producto, SUM(dp.cantidad) as Cantidad, sum(dp.sub_total) as Ingresos
                FROM public.pedido_producto dp
                    LEFT JOIN public.pedido p
                    ON dp.pedido_id=p.id
                    LEFT JOIN public.producto pr
                    ON pr.id=dp.producto_id
                    LEFT JOIN public.menu m
                    ON m.id=pr.menu_id
                    LEFT JOIN public.sucursal s
                    ON s.id=p.sucursal_id
                    where  p.estado_id=4 and p.fecha<= :FF and p.fecha>= :FI and s.admin_id= :admin group by pr.id, m.nombre, s.nombre order by s.nombre"; //agrupado producto todas las sucursales
                $consulta= $db->prepare($sql);
                $consulta->execute(array( 'FF' => $FF, 'FI' => $FI, 'admin' => $idAdmin ));//$params
                $resultProductosGeneral = $consulta->fetchAll();
            
            $sql= //numero de ventas// precio venta / menu/ agrupadas por sucursal  en periodo i - f 
            "SELECT  s.nombre as Sucursal, m.nombre as Menu, SUM(dp.cantidad) as Cantidad, sum(dp.sub_total) as Ingresos
                FROM public.pedido_producto dp
                    LEFT JOIN public.pedido p
                    ON dp.pedido_id=p.id
                    LEFT JOIN public.producto pr
                    ON pr.id=dp.producto_id
                    LEFT JOIN public.menu m
                    ON m.id=pr.menu_id
                    LEFT JOIN public.sucursal s
                    ON s.id=p.sucursal_id
                    where p.estado_id=4 and p.fecha<= :FF and p.fecha>= :FI and s.admin_id= :admin group by m.id, s.nombre order by s.nombre "; //agrupados por menu todas las sucursales
                $consulta= $db->prepare($sql);
                $consulta->execute(array( 'admin' => $idAdmin , 'FF' => $FF, 'FI' => $FI ));//$params
                $resultMenusGeneral = $consulta->fetchAll();
            
            $sql= //numero de ventas canceladas// precio venta / menu/ agrupadas por productos para las sucursales X en periodo i - f 
            "SELECT  s.nombre as Sucursal, m.nombre as menu, pr.nombre_producto , SUM(dp.cantidad) as Cantidad, sum(dp.sub_total) as Ingresos
                FROM public.pedido_producto dp
                    LEFT JOIN public.pedido p
                    ON dp.pedido_id=p.id
                    LEFT JOIN public.producto pr
                    ON pr.id=dp.producto_id
                    LEFT JOIN public.menu m
                    ON m.id=pr.menu_id
                    LEFT JOIN public.sucursal s
                    ON s.id=p.sucursal_id
                    where p.estado_id=5 and p.fecha<= :FF and p.fecha>= :FI and s.admin_id= :admin group by pr.id, m.nombre, s.nombre order by s.nombre"; //agrupados por menu todas las sucursales
                $consulta= $db->prepare($sql);
                $consulta->execute(array('admin' => $idAdmin ,   'FF' => $FF, 'FI' => $FI ));//$params
                $resultProductosCanceladosGeneral = $consulta->fetchAll();

            $sql= //numero de ventas canceladas // precio venta / menu/ agrupadas por menus para todas las sucursales X en periodo i - f 
            "SELECT  s.nombre as Sucursal, m.nombre as menu, SUM(dp.cantidad) as Cantidad, sum(dp.sub_total) as Ingresos
                FROM public.pedido_producto dp
                    LEFT JOIN public.pedido p
                    ON dp.pedido_id=p.id
                    LEFT JOIN public.producto pr
                    ON pr.id=dp.producto_id
                    LEFT JOIN public.menu m
                    ON m.id=pr.menu_id
                    LEFT JOIN public.sucursal s
                    ON s.id=p.sucursal_id
                    where s.admin_id= :admin and p.estado_id=5 and p.fecha<= :FF and p.fecha>= :FI group by m.id, m.nombre, s.nombre order by s.nombre"; // agrupados por menu una sucursal
                $consulta= $db->prepare($sql);
                $consulta->execute(array('admin' => $idAdmin, 'FF' => $FF, 'FI' => $FI ));//$params
                $resultMenusCanceladosGeneral = $consulta->fetchAll();
            
        //$entityManager = $this->getDoctrine()->getManager();
        //$query = $entityManager->createQuery('
        //      SELECT  m FROM App\Entity\Pedido m where m.sucursal = :sucursal '
        //    )->setParameters(array( 'sucursal' => $idSucursal ));
        //$result = $query->getArrayResult();

        $html = $this->render('reportesPdf/reporteVentas.html.twig', array( 
            'resultProductos' => $resultProductos,
            'resultMenu' => $resultMenu,
            'resultProductosCancelados' => $resultProductosCancelados,
            'resultMenusCancelados' => $resultMenusCancelados,
            'resultProductosGeneral' => $resultProductosGeneral,
            'resultMenusGeneral' => $resultMenusGeneral,
            'resultProductosCanceladosGeneral' => $resultProductosCanceladosGeneral,
            'resultMenusCanceladosGeneral' => $resultMenusCanceladosGeneral 
         ));
         return new PdfResponse( $snappy->getOutputFromHtml($html),'file.pdf', [
            'images' => true,
            'enable-javascript' => true,
            'page-size' => 'A4',
            'viewport-size' => '1280x1024',
        
            'margin-left' => '10mm',
            'margin-right' => '10mm',
            'margin-top' => '30mm',
            'margin-bottom' => '25mm',
          ] );
    }



    /**
     * @Route("/reportes/cantidadProductos", name="cantidadProductos")
     */
    public function cantidadProductos(\Knp\Snappy\Pdf $snappy)
    {

        $html = $this->render('graficos/graficosAdmin/graficosAdmin.html.twig', [  ]);

        return new PdfResponse( $snappy->getOutputFromHtml($html),'reporte.pdf' );

        //$webSiteURL = "https://www.chartjs.org/samples/latest/charts/bar/multi-axis.html";
        //$filname="asas";
        //return new Response(
        //    $snappy->getOutput($webSiteURL),
        // 200,
        // array(
        //    'Content-Type' => 'application/pdf',
        //    'Content-Disposition' => 'inline; filename="'.$filname.'.pdf"'
        // )
       //);
    }

 
    /**
     * @Route("/reporteEstadisticas", name="reporteEstadisticas", methods={"GET","POST"})
    */
    public function reporteVentas(Request $request, AdminSucursalRepository $AdminSucursalRepository )
    {
        $idSucursal= $request->get('sucursal');
        $FI = (new \DateTime($request->get('fechaInicial')))->format( 'Y-m-d' ); // '2019-12-01'; 
        $FF = (new \DateTime($request->get('fechaFinal')))->format( 'Y-m-d' ) ;  // '2019-12-31';
        $idUser= $this->getUser()->getId();

        $admin=$AdminSucursalRepository->findOneByUsuario($idUser);
        $idAdmin=$admin->getId();

        $em = $this->getDoctrine()->getManager();
        $db= $em->getConnection();
        //----------------------------------------------------------------------------------------------------------
        //SELECT id, pedido_id, producto_id, cantidad, sub_total FROM public.pedido_producto
        
        $sql= //numero de ventas/costo venta / precio venta / menu/ agrupadas por producto para sucursal X en periodo i - f 
        "SELECT  pr.nombre_producto as Producto, m.nombre as Menu,  SUM(dp.cantidad) as Cantidad, sum(dp.sub_total) as Ingresos
            FROM public.pedido_producto dp
                LEFT JOIN public.pedido p
                ON dp.pedido_id=p.id
                LEFT JOIN public.producto pr
                ON pr.id=dp.producto_id
                LEFT JOIN public.menu m
                ON m.id=pr.menu_id
                where p.sucursal_id  = :sucursal and p.estado_id=4 and p.fecha<= :FF and p.fecha>= :FI group by pr.id, m.nombre"; //agrupado producto una sucursal
            $consulta= $db->prepare($sql);
            $consulta->execute(array( 'sucursal' => $idSucursal, 'FF' => $FF, 'FI' => $FI ));//$params
            $resultProductos = $consulta->fetchAll();

            $sql= //numero de ventas// precio venta / menu/ agrupadas por menus para sucursal X en periodo i - f 
            "SELECT  m.nombre as Menu, SUM(dp.cantidad) as Cantidad, sum(dp.sub_total) as Ingresos
                FROM public.pedido_producto dp
                    LEFT JOIN public.pedido p
                    ON dp.pedido_id=p.id
                    LEFT JOIN public.producto pr
                    ON pr.id=dp.producto_id
                    LEFT JOIN public.menu m
                    ON m.id=pr.menu_id
                    where p.sucursal_id  = :sucursal and p.estado_id=4 and p.fecha<= :FF and p.fecha>= :FI group by m.id"; //agrupado menu una sucursal
                $consulta= $db->prepare($sql);
                $consulta->execute(array( 'sucursal' => $idSucursal, 'FF' => $FF, 'FI' => $FI ));//$params
                $resultMenu = $consulta->fetchAll();

            $sql= //numero de ventas canceladas// precio venta / menu/ agrupadas por productos para sucursal X en periodo i - f 
            "SELECT pr.nombre_producto as Producto , m.nombre as Menu, SUM(dp.cantidad) as Cantidad, sum(dp.sub_total) as Ingresos
            FROM public.pedido_producto dp
                LEFT JOIN public.pedido p
                ON dp.pedido_id=p.id
                LEFT JOIN public.producto pr
                ON pr.id=dp.producto_id
                LEFT JOIN public.menu m
                ON m.id=pr.menu_id
                where p.sucursal_id  = :sucursal and p.estado_id=5 and p.fecha<= :FF and p.fecha>= :FI group by pr.id, m.nombre"; //agrupados producto una sucursal
                $consulta= $db->prepare($sql);
                $consulta->execute(array( 'sucursal' => $idSucursal, 'FF' => $FF, 'FI' => $FI ));//$params
                $resultProductosCancelados = $consulta->fetchAll();

            $sql= //numero de ventas// precio venta / menu/ agrupadas por menus para sucursal X en periodo i - f 
            "SELECT  m.nombre as Menu, SUM(dp.cantidad) as Cantidad, sum(dp.sub_total) as Ingresos
                FROM public.pedido_producto dp
                    LEFT JOIN public.pedido p
                    ON dp.pedido_id=p.id
                    LEFT JOIN public.producto pr
                    ON pr.id=dp.producto_id
                    LEFT JOIN public.menu m
                    ON m.id=pr.menu_id
                    where p.sucursal_id  = :sucursal and p.estado_id=5 and p.fecha<= :FF and p.fecha>= :FI group by m.id"; // agrupados por menu una sucursal
                $consulta= $db->prepare($sql);
                $consulta->execute(array( 'sucursal' => $idSucursal, 'FF' => $FF, 'FI' => $FI ));//$params
                $resultMenusCancelados = $consulta->fetchAll();
//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
            $sql= //numero de ventas/costo venta / precio venta / menu/ agrupadas por producto todas las sucursales en periodo i - f 
            "SELECT s.nombre as Sucursal, m.nombre as Menu, pr.nombre_producto as Producto, SUM(dp.cantidad) as Cantidad, sum(dp.sub_total) as Ingresos
                FROM public.pedido_producto dp
                    LEFT JOIN public.pedido p
                    ON dp.pedido_id=p.id
                    LEFT JOIN public.producto pr
                    ON pr.id=dp.producto_id
                    LEFT JOIN public.menu m
                    ON m.id=pr.menu_id
                    LEFT JOIN public.sucursal s
                    ON s.id=p.sucursal_id
                    where  p.estado_id=4 and p.fecha<= :FF and p.fecha>= :FI and s.admin_id= :admin group by pr.id, m.nombre, s.nombre order by s.nombre"; //agrupado producto todas las sucursales
                $consulta= $db->prepare($sql);
                $consulta->execute(array( 'FF' => $FF, 'FI' => $FI, 'admin' => $idAdmin ));//$params
                $resultProductosGeneral = $consulta->fetchAll();
            
            $sql= //numero de ventas// precio venta / menu/ agrupadas por sucursal  en periodo i - f 
            "SELECT  s.nombre as Sucursal, m.nombre as Menu, SUM(dp.cantidad) as Cantidad, sum(dp.sub_total) as Ingresos
                FROM public.pedido_producto dp
                    LEFT JOIN public.pedido p
                    ON dp.pedido_id=p.id
                    LEFT JOIN public.producto pr
                    ON pr.id=dp.producto_id
                    LEFT JOIN public.menu m
                    ON m.id=pr.menu_id
                    LEFT JOIN public.sucursal s
                    ON s.id=p.sucursal_id
                    where p.estado_id=4 and p.fecha<= :FF and p.fecha>= :FI and s.admin_id= :admin group by m.id, s.nombre order by s.nombre "; //agrupados por menu todas las sucursales
                $consulta= $db->prepare($sql);
                $consulta->execute(array( 'admin' => $idAdmin , 'FF' => $FF, 'FI' => $FI ));//$params
                $resultMenusGeneral = $consulta->fetchAll();
            
            $sql= //numero de ventas canceladas// precio venta / menu/ agrupadas por productos para las sucursales X en periodo i - f 
            "SELECT  s.nombre as Sucursal, m.nombre as menu, pr.nombre_producto , SUM(dp.cantidad) as Cantidad, sum(dp.sub_total) as Ingresos
                FROM public.pedido_producto dp
                    LEFT JOIN public.pedido p
                    ON dp.pedido_id=p.id
                    LEFT JOIN public.producto pr
                    ON pr.id=dp.producto_id
                    LEFT JOIN public.menu m
                    ON m.id=pr.menu_id
                    LEFT JOIN public.sucursal s
                    ON s.id=p.sucursal_id
                    where p.estado_id=5 and p.fecha<= :FF and p.fecha>= :FI and s.admin_id= :admin group by pr.id, m.nombre, s.nombre order by s.nombre"; //agrupados por menu todas las sucursales
                $consulta= $db->prepare($sql);
                $consulta->execute(array('admin' => $idAdmin ,   'FF' => $FF, 'FI' => $FI ));//$params
                $resultProductosCanceladosGeneral = $consulta->fetchAll();

            $sql= //numero de ventas canceladas // precio venta / menu/ agrupadas por menus para todas las sucursales X en periodo i - f 
            "SELECT  s.nombre as Sucursal, m.nombre as menu, SUM(dp.cantidad) as Cantidad, sum(dp.sub_total) as Ingresos
                FROM public.pedido_producto dp
                    LEFT JOIN public.pedido p
                    ON dp.pedido_id=p.id
                    LEFT JOIN public.producto pr
                    ON pr.id=dp.producto_id
                    LEFT JOIN public.menu m
                    ON m.id=pr.menu_id
                    LEFT JOIN public.sucursal s
                    ON s.id=p.sucursal_id
                    where s.admin_id= :admin and p.estado_id=5 and p.fecha<= :FF and p.fecha>= :FI group by m.id, m.nombre, s.nombre order by s.nombre"; // agrupados por menu una sucursal
                $consulta= $db->prepare($sql);
                $consulta->execute(array('admin' => $idAdmin, 'FF' => $FF, 'FI' => $FI ));//$params
                $resultMenusCanceladosGeneral = $consulta->fetchAll();
            
        //$entityManager = $this->getDoctrine()->getManager();
        //$query = $entityManager->createQuery('
        //      SELECT  m FROM App\Entity\Pedido m where m.sucursal = :sucursal '
        //    )->setParameters(array( 'sucursal' => $idSucursal ));
        //$result = $query->getArrayResult();

        return $this->render('reportesPdf/reporteVentas.html.twig', array( 
            'resultProductos' => $resultProductos,
            'resultMenu' => $resultMenu,
            'resultProductosCancelados' => $resultProductosCancelados,
            'resultMenusCancelados' => $resultMenusCancelados,
            'resultProductosGeneral' => $resultProductosGeneral,
            'resultMenusGeneral' => $resultMenusGeneral,
            'resultProductosCanceladosGeneral' => $resultProductosCanceladosGeneral,
            'resultMenusCanceladosGeneral' => $resultMenusCanceladosGeneral 
         ));
    }
    
}


