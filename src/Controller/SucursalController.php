<?php

namespace App\Controller;

use App\Entity\Sucursal;
use App\Form\SucursalType;
use App\Repository\SucursalRepository;
use App\Repository\AdminSucursalRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
//---------------------SERVICIOS------------------------------
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;



/**
 * @Route("/sucursal")
 */
class SucursalController extends AbstractController
{

  /**
  * @Route("/editarAjax", name="editar_sucursal_Ajax", methods={"POST"})
  */

  public function EditarAjax(Request $request,  SucursalRepository $SucursalRepository){
    //$file = $request->files->get('upload');
    //$status = array('status' => "success","fileUploaded" => false);
    $arrayForm = $request->get('upload');
    $admin=$arrayForm["sucursal_Admin"];
    $entityManager = $this->getDoctrine()->getManager();
    $repository=$entityManager->getRepository('App:AdminSucursal');
    $Admin = $repository->findOneBy(array('id' => $admin));

        $sucursal = $SucursalRepository->findOneById( $arrayForm["id"]);
        $sucursal->setNombre($arrayForm["sucursal_nombre"]);
        $sucursal->setDireccion($arrayForm["sucursal_direccion"]);
        $sucursal->setAdmin($Admin);
      
        $jsonArray = array('success' => 'algo',);
        try {
            $entityManager->persist($sucursal);
            $entityManager->flush();
            $id= strval($sucursal->getId());
            $jsonArray = array('success' => 'true', 'id' =>  $id );
            return new JsonResponse($jsonArray);

        } catch (DBALException $e) {
            $message = sprintf('DBALException [%i]: %s', $e->getCode(), $e->getMessage());
            $jsonArray = array('success' => 'dbal', 'error' => $message);
        } catch (PDOException $e) {
            $message = sprintf('PDOException [%i]: %s', $e->getCode(), $e->getMessage());
            $jsonArray = array('success' => 'PDO', 'error' => $message);
        } catch (ORMException $e) {
            $message = sprintf('ORMException [%i]: %s', $e->getCode(), $e->getMessage());
            $jsonArray = array('success' => 'ORM', 'error' => $message);
        } catch (Exception $e) {
            $message = sprintf('Exception [%i]: %s', $e->getCode(), $e->getMessage());
            $jsonArray = array('success' => 'Excepcion', 'error' => $message);
        }
        return new JsonResponse($jsonArray);
  }

  /**
  * @Route("/cargarTabla", name="cargarTabla", methods={"POST"})
  */
    public function cargarTabla( AdminSucursalRepository $adminSucursalRepository,  SucursalRepository $SucursalRepository, Request $request ): Response
    {
      $user=null;
      //if( $this->getUser() ){
      //    $user=$this->getUser()->getId();
      //    $admin= $adminSucursalRepository->findOneByUsuario($user);
      //    $sucursals = $SucursalRepository->findByAdmin($admin);      
      // }else{
      //   $sucursals = $SucursalRepository->findById(-1);     
      // }
      $sucursals = $SucursalRepository->findAll(); 
        return $this->render('sucursal/sucursalTabla.html.twig', [
            'sucursals' => $sucursals,
        ]);
    }

  /**
  * @Route("/crearAjax", name="crear_Sucursal_Ajax", methods={"POST"})
  */

  public function crearAjax(Request $request){
    //$file = $request->files->get('upload');
    //$status = array('status' => "success","fileUploaded" => false);
    $arrayForm = $request->get('upload');
    $admin=$arrayForm["sucursal_Admin"];
    $entityManager = $this->getDoctrine()->getManager();
    $repository=$entityManager->getRepository('App:AdminSucursal');
    $Admin = $repository->findOneBy(array('id' => $admin));

        $sucursal = new Sucursal();
        $sucursal->setNombre($arrayForm["sucursal_nombre"]);
        $sucursal->setDireccion($arrayForm["sucursal_direccion"]);
        $sucursal->setAdmin($Admin);
      
        $jsonArray = array('success' => 'algo',);
          try {
            $entityManager->persist($sucursal);
            $entityManager->flush();
            $id= strval($sucursal->getId());
           
            $jsonArray = array('success' => 'true', 'id' =>  $id );
            return new JsonResponse($jsonArray);

        } catch (DBALException $e) {
            $message = sprintf('DBALException [%i]: %s', $e->getCode(), $e->getMessage());
            $jsonArray = array('success' => 'dbal', 'error' => $message);
        } catch (PDOException $e) {
            $message = sprintf('PDOException [%i]: %s', $e->getCode(), $e->getMessage());
            $jsonArray = array('success' => 'PDO', 'error' => $message);
        } catch (ORMException $e) {
            $message = sprintf('ORMException [%i]: %s', $e->getCode(), $e->getMessage());
            $jsonArray = array('success' => 'ORM', 'error' => $message);
        } catch (Exception $e) {
            $message = sprintf('Exception [%i]: %s', $e->getCode(), $e->getMessage());
            $jsonArray = array('success' => 'Excepcion', 'error' => $message);
        }

        return new JsonResponse($jsonArray);
  }
    /**
     * @Rest\Get("/sucursales/")
     *
    */
    public function servicio_sucursales(Request $request)
    {
      $entityManager = $this->getDoctrine()->getManager();
      $query = $entityManager->createQuery('
        SELECT d FROM App\Entity\Sucursal d');
      $md = $query->getArrayResult();
      $response = new Response();
      $response->setContent(json_encode($md));
      return $response;
    }

    /**
    * @Route("/render_sucursales", name="render_sucursales", methods={"POST"})
    */
    public function render_sucursales(Request $request, SucursalRepository $sucursalRepository){

        $sucursal = new Sucursal();
        $formSucursal = $this->createForm(SucursalType::class, $sucursal  );


        return $this->render('sucursal/index.html.twig', [
            'sucursals' => $sucursalRepository->findAll(),
            'formSucursal' => $formSucursal->createView(),
        ]);
    }

    /**
     * @Route("/", name="sucursal_index", methods={"GET"})
     */
    public function index(SucursalRepository $sucursalRepository): Response
    {
        return $this->render('sucursal/index.html.twig', [
            'sucursals' => $sucursalRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="sucursal_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $sucursal = new Sucursal();
        $form = $this->createForm(SucursalType::class, $sucursal);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($sucursal);
            $entityManager->flush();

            return $this->redirectToRoute('sucursal_index');
        }

        return $this->render('sucursal/new.html.twig', [
            'sucursal' => $sucursal,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="sucursal_show", methods={"GET"})
     */
    public function show(Sucursal $sucursal): Response
    {
        $usuario=$sucursal->getAdmin()->getUsuario()->getId();
        return $this->render('sucursal/show.html.twig', [
            'sucursal' => $sucursal,'admin' => $usuario,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="sucursal_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Sucursal $sucursal): Response
    {
        $form = $this->createForm(SucursalType::class, $sucursal);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('sucursal_index');
        }

        return $this->render('sucursal/edit.html.twig', [
            'sucursal' => $sucursal,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="sucursal_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Sucursal $sucursal): Response
    {
        if ($this->isCsrfTokenValid('delete'.$sucursal->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($sucursal);
            $entityManager->flush();
        }

        return $this->redirectToRoute('sucursal_index');
    }
}
