<?php

namespace App\Controller;

use App\Entity\Producto;
use App\Entity\Menu;
use App\Form\ProductoType;
use App\Form\MenuType;
use App\Repository\ProductoRepository;
use App\Repository\MenuRepository;
use App\Repository\SucursalRepository;
use App\Repository\AdminSucursalRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Doctrine\DBAL\DBALException;
use Doctrine\ORM\ORMException;
use Exception;
//---------------------SERVICIOS------------------------------
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;

use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactory;
use Symfony\Component\Serializer\Mapping\Loader\AnnotationLoader;
use Doctrine\Common\Annotations\AnnotationReader;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

/**
 * @Route("/producto")
 */
class ProductoController extends AbstractController
{

  /**
   * @Route("/render_productos_menu", name="render_productos_menu", methods={"POST"})
   */
  public function render_productos_menu(ProductoRepository $productoRepository, SucursalRepository $SucursalRepository, Request $request): Response
  {
    $producto = new Producto();
    $menu = new Menu();
    $formP = $this->createForm(ProductoType::class, $producto);
    $formM = $this->createForm(MenuType::class, $menu);
    //$form -> add('save', SubmitType::class);
    $formP->remove('image');
      return $this->render('producto/platillosMenu.html.twig', [
          'productos' => $productoRepository->findAll(array('id' => '')),
          'producto' => $producto,
          'formProducto' => $formP->createView(),
          'sucursals' => $SucursalRepository->findAll(array('id' => '')),
          'formMenu' => $formM->createView(),
      ]);
  }

/**
* @Route("/editar_producto_Ajax", name="editar_producto_Ajax", methods={"POST"})
*/

public function editar_producto_Ajax(Request $request){
  $file = $request->files->get('upload');
  $status = array('status' => "success","fileUploaded" => false);
  $arrayForm = $request->get('upload');
  $menu=$arrayForm["menu"];
  $idProducto = $arrayForm["idProducto"];
  $entityManager = $this->getDoctrine()->getManager();
  $repository=$entityManager->getRepository('App:Menu');
  $menu = $repository->findOneBy(array('id' => $menu));
  $repository2=$entityManager->getRepository('App:Producto');
  $producto = $repository2->findOneBy(array('id' => $idProducto));

   if(!is_null($file)){ //&& $arrayForm["nombreProducto"]!=null
       $producto->setImageFile($file["file"]);
   }
      $producto->setNombreProducto($arrayForm["nombreProducto"]);
      $producto->setDescripcion($arrayForm["descripcion"]);
      $producto->setPrecioCompra($arrayForm["precioCompra"]);
      $producto->setPrecioVenta($arrayForm["precioVenta"]);
      $producto->setImagen($arrayForm["imagen"]);
      $producto->setMenu($menu);

      $jsonArray = array('success' => 'algo',);
        try {
          $entityManager->persist($producto);
          $entityManager->flush();
          $id= strval($producto->getId());
          $query = $entityManager->createQuery("SELECT u FROM App\Entity\Producto u WHERE   u.id= :id ");
          $query->setParameters( array('id' => $id ));
          $producto = $query->getResult();
          $jsonArray = array('success' => 'true', 'id' => $id, 'menu' => $arrayForm["menu"]);
          return new JsonResponse($jsonArray);

      } catch (DBALException $e) {
          $message = sprintf('DBALException [%i]: %s', $e->getCode(), $e->getMessage());
          $jsonArray = array('success' => 'dbal', 'error' => $message);
      } catch (PDOException $e) {
          $message = sprintf('PDOException [%i]: %s', $e->getCode(), $e->getMessage());
          $jsonArray = array('success' => 'PDO', 'error' => $message);
      } catch (ORMException $e) {
          $message = sprintf('ORMException [%i]: %s', $e->getCode(), $e->getMessage());
          $jsonArray = array('success' => 'ORM', 'error' => $message);
      } catch (Exception $e) {
          $message = sprintf('Exception [%i]: %s', $e->getCode(), $e->getMessage());
          $jsonArray = array('success' => 'Excepcion', 'error' => $message);
      }

      return new JsonResponse($jsonArray);

   $jsonArray = array('success' => 'no hay imagen',);
   return new JsonResponse($jsonArray);
}

  /**
  * @Route("/json_producto", name="json_producto", methods={"POST"})
  */
  public function json_producto(Request $request){
      $id = intval($request->get('id'));
      $em = $this->getDoctrine()->getManager();

      $repository = $this->getDoctrine()->getManager()->getRepository('App:Producto');
      $producto = $repository->findOneBy(array('id' => $id));
      $jsonArray = array(

          'nombreProducto' =>  $producto->getNombreProducto(),
          'descripcion' =>  $producto->getDescripcion(),
          'precioCompra' =>  $producto->getPrecioCompra(),
          'precioVenta' =>  $producto->getPrecioVenta(),
          'imagen' =>  $producto->getImagen(),
          'menu' =>  $producto->getMenu()->getId(),
      );
      return new JsonResponse($jsonArray);
  }

  /**
  * @Route("/pintar_uno_ajax", name="pintar_uno_ajax", methods={"POST"})
  */
  public function pintar_uno_ajax(Request $request){
      $id = intval($request->get('id'));
      $em = $this->getDoctrine()->getManager();
      $query = $em->createQuery("SELECT u FROM App\Entity\Producto u WHERE u.id= :id ");
      $query->setParameters( array('id' => $id ));
      $producto = $query->getResult();
       return $this->render('producto/pintarProductos.html.twig', [
           'productos' => $producto,
       ]);
  }

  /**
  * @Route("/eliminar_producto_ajax", name="eliminar_producto_ajax", methods={"POST"})
  */
  public function eliminar_producto_ajax(Request $request){
    $id = $request->get('id');
    $entityManager = $this->getDoctrine()->getManager();
    $repository = $entityManager->getRepository('App:Producto');
    $entidad = $repository->findOneBy(array('id' => $id));
    $entityManager->remove($entidad);
    $entityManager->flush();
       $jsonArray = array(
           'success' => true,
       );
       return new JsonResponse($jsonArray);
  }

  /**
  * @Route("/filtrar_productos_ajax", name="filtrar_productos_ajax", methods={"POST"})
  */
  public function filtrar_productos_ajax(Request $request){
      $menu = $request->get('menu');

      $em = $this->getDoctrine()->getManager();
      $query = $em->createQuery(
      "SELECT u FROM App\Entity\Producto u WHERE   u.menu= :menu ");
      $query->setParameters( array('menu' => $menu ));
      $producto = $query->getResult();

       return $this->render('producto/pintarProductos.html.twig', [
           'productos' => $producto,
       ]);
  }

  /**
  * @Route("/cargar_menu_ajax", name="cargar_menu_ajax", methods={"POST"})
  */
  public function cargar_menu_ajax(Request $request){
    $sucursal = $request->get('sucursal');
    $em = $this->getDoctrine()->getManager();
      $query = $em->createQuery(
      "SELECT p
      FROM App:Menu p
      WHERE ( p.idSucursal = :sucursal )
      ORDER BY p.id asc"
      );
      $menus = $query->setParameters( array( 'sucursal' =>$sucursal,))->getResult();
       return $this->render('menu/index.html.twig', [
           'menus' => $menus,
       ]);
  }

  /**
   * @Route("/", name="producto_index", methods={"GET"})
   */
  public function index( AdminSucursalRepository $adminSucursalRepository, ProductoRepository $productoRepository, SucursalRepository $SucursalRepository, MenuRepository $menuRepository, Request $request ): Response
  {
    $user=null;

    if( $this->getUser() ){
        $user=$this->getUser()->getId();
        $admin= $adminSucursalRepository->findOneByUsuario($user);
        $sucursals = $SucursalRepository->findByAdmin($admin);      
     }else{
        $sucursals = $SucursalRepository->findById(-1);     
     }
    
      return $this->render('producto/index.html.twig', [

          'sucursals' => $sucursals,

      ]);
  }

  /**
  * @Route("/buscarAjax", name="buscar_Ajax", methods={"POST"})
  */
  public function buscarAjax(Request $request){
    $nombre = $request->get('frase');

      $em = $this->getDoctrine()->getManager();
      $query = $em->createQuery("SELECT u FROM App\Entity\Producto u WHERE   u.nombreProducto LIKE :nombreProducto ");
      $query->setParameters( array('nombreProducto' => '%'.$nombre.'%' ));
      $producto = $query->getResult();

       return $this->render('producto/pintarProductos.html.twig', [
           'productos' => $producto,
       ]);
  }

  /**
  * @Route("/verificarNombre", name="verificar_Nombre", methods={"POST"})
  */
  public function verificarNombre(Request $request){
    $nombre = $request->get('producto');

    $em = $this->getDoctrine()->getManager();
        $query = $em->createQuery("SELECT u.nombreProducto as nombre FROM App\Entity\Producto u WHERE (u.nombreProducto = :nombreProducto )");
        $query->setParameters( array('nombreProducto' => $nombre ));
        $producto = $query->getResult();
        if($producto == null){
            $jsonArray = array('data' => 'Bien', );
            return new JsonResponse($jsonArray);
        }
        else{
            $jsonArray = array('data' => 'repetido', );
            return new JsonResponse($jsonArray);
        }
  }

  /**
  * @Route("/crearAjax", name="crear_Ajax", methods={"POST"})
  */

  public function crearAjax(Request $request){
    $file = $request->files->get('upload');
    $status = array('status' => "success","fileUploaded" => false);
    $arrayForm = $request->get('upload');
    $menu=$arrayForm["menu"];
    $entityManager = $this->getDoctrine()->getManager();
    $repository=$entityManager->getRepository('App:Menu');
    $menu = $repository->findOneBy(array('id' => $menu));

     if(!is_null($file)  ){ //&& $arrayForm["nombreProducto"]!=null
        $producto = new Producto();
        $producto->setNombreProducto($arrayForm["nombreProducto"]);
        //$producto->setCantidad($arrayForm["cantidad"]);
        $producto->setDescripcion($arrayForm["descripcion"]);
        $producto->setPrecioCompra($arrayForm["precioCompra"]);
        $producto->setPrecioVenta($arrayForm["precioVenta"]);
        $producto->setImagen($arrayForm["imagen"]);
        $producto->setMenu($menu);
        $producto->setImageFile($file["file"]);

        $jsonArray = array('success' => 'algo',);
          try {
            $entityManager->persist($producto);
            $entityManager->flush();
            $id= strval($producto->getId());
            $query = $entityManager->createQuery("SELECT u FROM App\Entity\Producto u WHERE   u.id= :id ");
            $query->setParameters( array('id' => $id ));
            $producto = $query->getResult();
            $jsonArray = array('success' => 'true', 'id' => $id, 'menu' => $arrayForm["menu"]);
            return new JsonResponse($jsonArray);

        } catch (DBALException $e) {
            $message = sprintf('DBALException [%i]: %s', $e->getCode(), $e->getMessage());
            $jsonArray = array('success' => 'dbal', 'error' => $message);
        } catch (PDOException $e) {
            $message = sprintf('PDOException [%i]: %s', $e->getCode(), $e->getMessage());
            $jsonArray = array('success' => 'PDO', 'error' => $message);
        } catch (ORMException $e) {
            $message = sprintf('ORMException [%i]: %s', $e->getCode(), $e->getMessage());
            $jsonArray = array('success' => 'ORM', 'error' => $message);
        } catch (Exception $e) {
            $message = sprintf('Exception [%i]: %s', $e->getCode(), $e->getMessage());
            $jsonArray = array('success' => 'Excepcion', 'error' => $message);
        }

        return new JsonResponse($jsonArray);
     }
     $jsonArray = array('success' => 'no hay imagen',);
     return new JsonResponse($jsonArray);

  }

    /**
     * @Route("/new", name="producto_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $producto = new Producto();
        $form = $this->createForm(ProductoType::class, $producto);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($producto);
            $entityManager->flush();

            return $this->redirectToRoute('producto_new');
        }

        return $this->render('producto/new.html.twig', [
            'producto' => $producto,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="producto_show", methods={"GET"})
     */
    public function show(Producto $producto): Response
    {
        return $this->render('producto/show.html.twig', [
            'producto' => $producto,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="producto_edit", methods={"GET","POST"})
     */
     public function edit(Request $request, Producto $producto): Response
     {

         $form = $this->createForm(ProductoType::class, $producto);
         $form -> add('save', SubmitType::class);
         $form -> remove('image');
         $form->handleRequest($request);
         $message='';
         $estado='Ingrese los cambios';

         if ($form->isSubmitted() && $form->isValid()) {


             try {
                 $this->getDoctrine()->getManager()->flush();
                 $estado='Listo';
             } catch (DBALException $e) {
                 $message = sprintf('DBALException [%i]: %s', $e->getCode(), $e->getMessage());
                 $estado='ERROR EN dbal';
             } catch (PDOException $e) {
                 $message = sprintf('PDOException [%i]: %s', $e->getCode(), $e->getMessage());
                 $estado='ERROR EN: PDO';
             } catch (ORMException $e) {
                 $message = sprintf('ORMException [%i]: %s', $e->getCode(), $e->getMessage());
                 $estado='ERROR en ORM';
             } catch (Exception $e) {
                 $message = sprintf('Exception [%i]: %s', $e->getCode(), $e->getMessage());
                 $estado='Excepcion';
             }
             return $this->render('producto/edit.html.twig', [
                 'producto' => $producto,
                 'form' => $form->createView(),
                 'estado'=> $estado,
                 'mensaje'=> $message,
             ]);

         }

         return $this->render('producto/edit.html.twig', [
             'producto' => $producto,
             'form' => $form->createView(),
             'estado'=> $estado,
             'mensaje'=> $message,
         ]);
     }

    /**
     * @Route("/{id}", name="producto_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Producto $producto): Response
    {
        if ($this->isCsrfTokenValid('delete'.$producto->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($producto);
            $entityManager->flush();
        }

        //return $this->redirectToRoute('producto_index');
        return new Response(
           '<html><body>Eliminado</body></html>'
       );
    }

    /**
     * @Route("/{id}/pato", name="pato", methods={"GET","POST"})
     */
     public function pato(Request $request): Response
     {
         return $this->render('patoTemplate/index.html.twig');
     }
}
