<?php

namespace App\Controller;

use App\Entity\Pedido;
use App\Entity\Producto;
use App\Entity\Menu;
use App\Form\PedidoType;
use App\Repository\PedidoRepository;
use App\Repository\EstadoRepository;
use App\Repository\SucursalRepository;
use App\Repository\ProductoRepository;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Form\ProductoType;
use App\Form\MenuType;

use App\Entity\PedidoProducto;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\Validator\Constraints\DateTime;

use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactory;
use Symfony\Component\Serializer\Mapping\Loader\AnnotationLoader;
use Doctrine\Common\Annotations\AnnotationReader;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\PropertyInfo\Extractor\ReflectionExtractor;

class ServiciosPedidoController extends FOSRestController
{

    /**
   * @Rest\Get("api/cancelarPedido/{id}")
   *
  */
  public function cancelarPedido(Request $request, PedidoRepository $pedidoRepository, EstadoRepository $estadoRepository  )
  {
    $id = (int)$request->get('id');
    $pedido = $pedidoRepository->findOneById($id);
    $estado = $estadoRepository->findOneById(5);

    if($pedido->getEstado()->getId()<4){
      $pedido->setEstado(5);
      $entityManager = $this->getDoctrine()->getManager();
      $entityManager->persist($pedido);
      $entityManager->flush();
      $array= array('estado' => 'true'  );
      $response = new Response();
      $response->setContent( json_encode($array));
    }else{
      $array= array('estado' => 'false', 'error' => 'este pedido ya ha finalizado'  );
      $response = new Response();
      $response->setContent( json_encode($array));
    }
    
    return $response;
    
  }

  /**
   * @Rest\Get("api/pedirDetallePedido/{id}")
   *
  */
  public function ServicioDetallesPedido(Request $request, SerializerInterface $serializer )
  {
    $id = (int)$request->get('id');
    $entityManager = $this->getDoctrine()->getManager();
    $query = $entityManager->createQuery('
    SELECT  m FROM App\Entity\Pedido m where m.id =:id')->setParameters(array('id' => $id ));
      $md = $query->getResult();

      $classMetadataFactory = new ClassMetadataFactory(new AnnotationLoader(new AnnotationReader()));
      $normalizer = new ObjectNormalizer($classMetadataFactory);
      $serializer = new Serializer([new DateTimeNormalizer(),$normalizer]);

      $data = $serializer->normalize($md, 'json', ['groups' => 'productosPedido']);
      //-----------RESPUESTA-------------------------------
      $response = new Response();
      $response->setContent(json_encode($data));
      return $response;
  }

  /**
  * Creates an Article resource
  * @Rest\Post("api/log/enviarPedidos/")
  * @param Request $request
  * @return Json
  */
  public function enviarPedidos(Request $request)
  {
    //$data=$request->getContent();
    //$sucursals = $SucursalRepository->findAll();
    $em = $this->getDoctrine()->getManager();
    $objPedido= new Pedido();
    $fecha = new \DateTime();
    
    $sucursal = $request->get('sucursal');
    $productos = $request->get('productos');
    $idUsuario =  (int) $this->get('security.token_storage')->getToken()->getUser()->getUsername(); 
    //return $idUsuario;
    $user= $em->getRepository('App\Entity\User')->findOneById( $idUsuario );
    $ObjCliente=  $em->getRepository('App\Entity\UsuarioClientes')->findOneById($user->getUsuario());
   
    $response = new Response();
    $response->setContent( json_encode($user));
    //return $response;
    $ObjSucursal= $em->getRepository('App\Entity\Sucursal')       ->findOneBy(array('id' => $sucursal ));
    $ObjEstado=   $em->getRepository('App\Entity\Estado')         ->findOneBy(array('id' => 1 ));

    $repoProductos = $em->getRepository('App\Entity\Producto');

    $objPedido->setFecha($fecha);
    $objPedido->setCliente($ObjCliente);
    $objPedido->setSucursal($ObjSucursal);
    $objPedido->setEstado($ObjEstado);
    $total=0;
    foreach($productos as $pedido){
      $objPedidoProducto= new PedidoProducto();
      $p=$repoProductos->findOneBy(array('id' => $pedido["producto"] ));
      $objPedidoProducto->setProducto($p);
      $objPedidoProducto->setCantidad( $pedido["cantidad"] );
      $subTotal= $objPedidoProducto->getCantidad()*$p->getPrecioVenta();
      $objPedidoProducto->setSubTotal($subTotal);
      $objPedido->addProductos($objPedidoProducto);
      $total= $total+($subTotal);
    }
    $objPedido->setTotal($total);
    $em->persist($objPedido);
    $em->flush();
    $array= array('total' => $total ,'idPedido' => $objPedido->getId() );
    $response = new Response();
    $response->setContent( json_encode($array));
    return $response;
  }
  /**
   * @Rest\POST("api/pedidos/")
   *
  */
  public function listaPedidos(Request $request, SerializerInterface $serializer )
  {
    
    $id = (int) $this->get('security.token_storage')->getToken()->getUser()->getUsername();

    $entityManager = $this->getDoctrine()->getManager();
    $query = $entityManager->createQuery('
    SELECT  p
      FROM App\Entity\Pedido p
        INNER JOIN App\Entity\UsuarioClientes c
        WITH p.cliente = c.id
            INNER JOIN App\Entity\User u
            WITH c.Usuario = u.id
        WHERE (u.id = :id)
        order by p.id desc'
        )->setParameters(array('id' => $id ));
      $md = $query->getResult();
      
      //-----------RESPUESTA-------------------------------
      $classMetadataFactory = new ClassMetadataFactory(new AnnotationLoader(new AnnotationReader()));
      $normalizer = new ObjectNormalizer($classMetadataFactory);
      $serializer = new Serializer([new DateTimeNormalizer(),$normalizer]);

      $data = $serializer->normalize($md, 'json', ['groups' => 'ListaPedidos']);
      
      //-----------RESPUESTA-------------------------------
      $response = new Response();
      $response->setContent(
        json_encode($data)
      );
      return $response;
  }

  
}
