<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;



class GraficosController extends AbstractController
{

  /**
  * @Route("/graficos/render_graficos_index", name="render_graficos_index", methods={"POST"})
  */
  public function render_graficos_index(Request $request){

    return $this->render('graficos/graficosAdmin/graficosAdmin.html.twig', [  ]
    );
  }

    /**
     * @Route("/graficos", name="graficos")
     */
    public function index()
    {
        return $this->render('graficos/index.html.twig', [
            'controller_name' => 'GraficosController',
        ]);
    }
}
