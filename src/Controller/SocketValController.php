<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\User;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * @Route("/socketval")
 */
class SocketValController extends AbstractController
{


    /**
    * @Route("/pedir_token", name="pedir_token", methods={"POST"})
    */
    public function getToken(){
        $em = $this->getDoctrine()->getManager();
        //$query = $em->createQuery("SELECT u FROM App\Entity\Producto u WHERE   u.nombreProducto LIKE :nombreProducto ");
        //$query->setParameters( array('nombreProducto' => '%'.$nombre.'%' ));
        //$producto = $query->getResult();
        $user=$this->getUser();
        $user->settokenSocket();
        $em->persist($user);
        $em->flush();

        $jsonArray = array('data' => $this->getUser()->gettokenSocket(), );
        return new JsonResponse($jsonArray);
    }
}
