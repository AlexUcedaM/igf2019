<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\MaxDepth;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MensajeRepository")
  * normalizationContext={"groups"={ "GMensajesUsuario"}}
 */
class Mensaje
{

  /**
    * @ORM\Id()
    * @ORM\Column(type="integer", nullable=false)
    * @ORM\GeneratedValue(strategy="IDENTITY")
    * @ORM\SequenceGenerator(sequenceName="id_seq", allocationSize=1, initialValue=1)
    * @Groups({ "GmensajeInvert" })
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=250)
     * @Groups({ "GmensajeInvert" })
     */
    private $contenido;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="enviados")
     * @ORM\JoinColumn(nullable=false)
     * @MaxDepth(2)
     * @Groups({ "GmensajeInvert"})
     */
    private $idUserA;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="mensajes")
     * @ORM\JoinColumn(nullable=false)
     * @MaxDepth(2)
     * @Groups({ "GmensajeInvert" })
     */
    private $idUserB;

    /**
     * @ORM\Column(type="datetime")
     * @Groups({"GmensajeInvert"})
     */
    private $fecha;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Imagen", cascade={"persist", "remove"})
     */
    private $imagen;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Sucursal", inversedBy="mensajes")
     */
    private $sucursal;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getContenido(): ?string
    {
        return $this->contenido;
    }

    public function setContenido(?string $contenido): self
    {
        $this->contenido = $contenido;

        return $this;
    }

    public function getIdUserA(): ?User
    {
        return $this->idUserA;
    }

    public function setIdUserA(?User $idUserA): self
    {
        $this->idUserA = $idUserA;

        return $this;
    }

    public function getIdUserB(): ?User
    {
        return $this->idUserB;
    }

    public function setIdUserB(?User $idUserB): self
    {
        $this->idUserB = $idUserB;

        return $this;
    }


    public function getFecha(): ?\DateTimeInterface
    {
        return $this->fecha;
    }

    public function setFecha(\DateTimeInterface $fecha): self
    {
        $this->fecha = $fecha;

        return $this;
    }

    public function getImagen(): ?imagen
    {
        return $this->imagen;
    }

    public function setImagen(?imagen $imagen): self
    {
        $this->imagen = $imagen;

        return $this;
    }

    public function getSucursal(): ?Sucursal
    {
        return $this->sucursal;
    }

    public function setSucursal(?Sucursal $sucursal): self
    {
        $this->sucursal = $sucursal;

        return $this;
    }

}
