<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MenuRepository")
 */
class Menu
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"MenusProductos" })
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=30, unique=true)
     * @Groups({"MenusProductos" })
     */
    private $nombre;

    /**
     * @ORM\Column(type="string", length=150, nullable=true)
     * @Groups({"MenusProductos" })
     */
    private $descripcion;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Sucursal", inversedBy="yes")
     */
    private $idSucursal;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Producto", mappedBy="menu")
     * @Groups({"MenusProductos" })
     */
    private $productos;

    public function __construct()
    {
        $this->productos = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getDescripcion(): ?string
    {
        return $this->descripcion;
    }

    public function setDescripcion(?string $descripcion): self
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    public function getIdSucursal(): ?Sucursal
    {
        return $this->idSucursal;
    }

    public function setIdSucursal(?Sucursal $idSucursal): self
    {
        $this->idSucursal = $idSucursal;

        return $this;
    }

    /**
     * @return Collection|Producto[]
     */
    public function getProductos(): Collection
    {
        return $this->productos;
    }

    public function addProducto(Producto $producto): self
    {
        if (!$this->productos->contains($producto)) {
            $this->productos[] = $producto;
            $producto->setMenu($this);
        }

        return $this;
    }

    public function removeProducto(Producto $producto): self
    {
        if ($this->productos->contains($producto)) {
            $this->productos->removeElement($producto);
            // set the owning side to null (unless already changed)
            if ($producto->getMenu() === $this) {
                $producto->setMenu(null);
            }
        }

        return $this;
    }
}
