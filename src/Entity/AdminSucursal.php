<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AdminSucursalRepository")
 */
class AdminSucursal
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\User", inversedBy="adminSucursal", cascade={"persist", "remove"})
     * @Groups({ "ListaPedidos", "listaChats" })  
     */
    private $usuario;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Sucursal", mappedBy="admin")
     */
    private $sucursals;

    public function __construct()
    {
        $this->sucursals = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUsuario(): ?User
    {
        return $this->usuario;
    }

    public function setUsuario(?User $usuario): self
    {
        $this->usuario = $usuario;

        return $this;
    }

    /**
     * @return Collection|Sucursal[]
     */
    public function getSucursals(): Collection
    {
        return $this->sucursals;
    }

    public function addSucursal(Sucursal $sucursal): self
    {
        if (!$this->sucursals->contains($sucursal)) {
            $this->sucursals[] = $sucursal;
            $sucursal->setAdmin($this);
        }

        return $this;
    }

    public function removeSucursal(Sucursal $sucursal): self
    {
        if ($this->sucursals->contains($sucursal)) {
            $this->sucursals->removeElement($sucursal);
            // set the owning side to null (unless already changed)
            if ($sucursal->getAdmin() === $this) {
                $sucursal->setAdmin(null);
            }
        }

        return $this;
    }
}
