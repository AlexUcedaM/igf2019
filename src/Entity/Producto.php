<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProductoRepository")
* @Vich\Uploadable
 */
class Producto
{

    /**
    * @ORM\Column(type="string", length=255, unique=true)
    * @var string
    * @Groups({"MenusProductos" })
    */
   private $image;

   /**
    * @Vich\UploadableField(mapping="product_images", fileNameProperty="image")
    * @var File
    */
   private $imageFile;

    /**
     * @ORM\Id()
     * @Groups({"MenusProductos" })
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50, unique=true)
     * @Groups({"MenusProductos", "productosPedido" })
     */
    private $nombreProducto;

    /**
     * @ORM\Column(type="text", length=500, nullable=true)
     * @Groups({"MenusProductos" })
     */
    private $descripcion;

    /**
     * @ORM\Column(type="integer", nullable=true )
     * @Groups({"MenusProductos" })
     */
    private $cantidad;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $SKU;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $precioCompra;

    /**
     * @ORM\Column(type="float")
     * @Groups({"MenusProductos" })
     */
    private $precioVenta;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $imagen;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Menu", inversedBy="productos")
     */
    private $menu;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\PedidoProducto", mappedBy="producto")
     */
    private $pedidos;


    public function __construct()
    {
        $this->pedidos = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNombreProducto(): ?string
    {
        return $this->nombreProducto;
    }

    public function setNombreProducto(string $nombreProducto): self
    {
        $this->nombreProducto = $nombreProducto;

        return $this;
    }

    public function getCantidad(): ?int
    {
        return $this->cantidad;
    }

    public function setCantidad(int $cantidad): self
    {
        $this->cantidad = $cantidad;

        return $this;
    }

    public function getSKU(): ?string
    {
        return $this->SKU;
    }

    public function setSKU(?string $SKU): self
    {
        $this->SKU = $SKU;

        return $this;
    }

    public function getPrecioCompra(): ?float
    {
        return $this->precioCompra;
    }

    public function setPrecioCompra(float $precioCompra): self
    {
        $this->precioCompra = $precioCompra;

        return $this;
    }

    public function getPrecioVenta(): ?float
    {
        return $this->precioVenta;
    }

    public function setPrecioVenta(float $precioVenta): self
    {
        $this->precioVenta = $precioVenta;

        return $this;
    }

    public function getImagen(): ?string
    {
        return $this->imagen;
    }

    public function setImagen(?string $imagen): self
    {
        $this->imagen = $imagen;

        return $this;
    }



    public function setImageFile(File $image = null)
 {
     $this->imageFile = $image;

     // VERY IMPORTANT:
     // It is required that at least one field changes if you are using Doctrine,
     // otherwise the event listeners won't be called and the file is lost
     if ($image) {
         // if 'updatedAt' is not defined in your entity, use another property
         $this->updatedAt = new \DateTime('now');
     }
 }

 public function getImageFile()
                                  {
                                       return $this->imageFile;
                                 }

 public function setImage($image)
                                  {
                                     $this->image = $image;
                                   }

 public function getImage()
                                                                                  {
                                                                                      return $this->image;
                                                                                  }

    public function getDescripcion(): ?string
    {
        return $this->descripcion;
    }

    public function setDescripcion(string $descripcion): self
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    public function getMenu(): ?Menu
    {
        return $this->menu;
    }

    public function setMenu(?Menu $menu): self
    {
        $this->menu = $menu;

        return $this;
    }

    public function __toString()
    {
        return $this->getNombreProducto();
    }

    /**
     * @return Collection|PedidoProducto[]
     */
    public function getPedidos(): Collection
    {
        return $this->pedidos;
    }

    public function addPedido(PedidoProducto $pedido): self
    {
        if (!$this->pedidos->contains($pedido)) {
            $this->pedidos[] = $pedido;
            $pedido->setProducto($this);
        }

        return $this;
    }

    public function removePedido(PedidoProducto $pedido): self
    {
        if ($this->pedidos->contains($pedido)) {
            $this->pedidos->removeElement($pedido);
            // set the owning side to null (unless already changed)
            if ($pedido->getProducto() === $this) {
                $pedido->setProducto(null);
            }
        }

        return $this;
    }



}
