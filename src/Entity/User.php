<?php
// src/Entity/User.php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\MaxDepth;
/**
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 * normalizationContext={"groups"={"Gmensaje", "productosPedido" }
 * denormalizationContext={"groups"={}}
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @Groups({"GmensajeInvert", "listaChats" })
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({ "ListaPedidos" })
     */
    protected $id;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\UsuarioClientes", mappedBy="Usuario", cascade={"persist", "remove"})
     * @Groups({"GmensajeInvert" })
     */
    private $usuario;

   /**
   * @ORM\Column(type="text", length=50, nullable=true)
   * @Groups({"GmensajeInvert" })
   */
   private $nombres;

   /**
   * @ORM\Column(type="text", length=50, nullable=true)
   */
   private $tokenSocket;

   /**
   * @ORM\Column(type="text", length=50, nullable=true)
   */
   private $nit;

   /**
   * @ORM\Column(type="text", length=50, nullable=true)
   */
   private $dui;

   /**
   * @ORM\Column(type="text", length=50, nullable=true)

   */
   private $apellidos;

   /**
   * @ORM\Column(type="text", length=50, nullable=true)

   */
   private $direccion;

   /**
    * @ORM\OneToOne(targetEntity="App\Entity\AdminSucursal", mappedBy="usuario", cascade={"persist", "remove"})
    */
   private $adminSucursal;

   /**
    * @ORM\OneToOne(targetEntity="App\Entity\AdminSistema", mappedBy="usuarioSistema", cascade={"persist", "remove"})
    */
   private $adminSistema;

   /**
    * @ORM\OneToMany(targetEntity="App\Entity\Mensaje", mappedBy="idUserB")
    * @MaxDepth(3)
    */
   private $mensajes;

   /**
    * @ORM\OneToMany(targetEntity="App\Entity\Mensaje", mappedBy="idUserA")
    */
   private $enviados;

   public function getNombres(): ?string{
       return $this->nombres;
   }
   public function setNombres(?string $nombres){
       $this->nombres = $nombres;
   }

   public function gettokenSocket(): ?string{
       return $this->tokenSocket;
   }
   public function settokenSocket(){
       $this->tokenSocket = bin2hex(openssl_random_pseudo_bytes(64));
   }

   public function getApellidos(): ?string{
       return $this->apellidos;
   }
   public function setApellidos(?string $apellidos){
       $this->apellidos = $apellidos;
   }
   public function getDireccion(): ?string{
       return $this->direccion;
   }
   public function setDireccion(?string $direccion){
       $this->direccion = $direccion;
   }



    public function __construct()
    {
        parent::__construct();
        $this->mensajes = new ArrayCollection();
        $this->enviados = new ArrayCollection();
        // your own logic
    }
    public function getId(): ?int
    {
        return $this->id;
    }
    public function getUsuario(): ?UsuarioClientes
    {
        return $this->usuario;
    }
    public function setUsuario(?UsuarioClientes $usuario): self
    {
        $this->usuario = $usuario;

        // set (or unset) the owning side of the relation if necessary
        $newUsuario = $usuario === null ? null : $this;
        if ($newUsuario !== $usuario->getUsuario()) {
            $usuario->setUsuario($newUsuario);
        }

        return $this;
    }

    public function getNit(): ?string
    {
        return $this->nit;
    }

    public function setNit(?string $nit): self
    {
        $this->nit = $nit;

        return $this;
    }

    public function getDui(): ?string
    {
        return $this->dui;
    }

    public function setDui(?string $dui): self
    {
        $this->dui = $dui;

        return $this;
    }

    public function getAdminSucursal(): ?AdminSucursal
    {
        return $this->adminSucursal;
    }

    public function setAdminSucursal(?AdminSucursal $adminSucursal): self
    {
        $this->adminSucursal = $adminSucursal;

        // set (or unset) the owning side of the relation if necessary
        $newUsuario = $adminSucursal === null ? null : $this;
        if ($newUsuario !== $adminSucursal->getUsuario()) {
            $adminSucursal->setUsuario($newUsuario);
        }

        return $this;
    }

    public function getAdminSistema(): ?AdminSistema
    {
        return $this->adminSistema;
    }

    public function setAdminSistema(?AdminSistema $adminSistema): self
    {
        $this->adminSistema = $adminSistema;

        // set (or unset) the owning side of the relation if necessary
        $newUsuarioSistema = $adminSistema === null ? null : $this;
        if ($newUsuarioSistema !== $adminSistema->getUsuarioSistema()) {
            $adminSistema->setUsuarioSistema($newUsuarioSistema);
        }

        return $this;
    }

    /**
     * @return Collection|Mensaje[]
     */
    public function getMensajes(): Collection
    {
        return $this->mensajes;
    }

    public function addMensajes(Mensaje $mensajes): self
    {
        if (!$this->mensajes->contains($mensajes)) {
            $this->mensajes[] = $mensajes;
            $mensajes->setIdUserB($this);
        }

        return $this;
    }

    public function removeMensajes(Mensaje $mensajes): self
    {
        if ($this->mensajes->contains($mensajes)) {
            $this->mensajes->removeElement($mensajes);
            // set the owning side to null (unless already changed)
            if ($mensajes->getIdUserB() === $this) {
                $mensajes->setIdUserB(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Mensaje[]
     */
    public function getEnviados(): Collection
    {
        return $this->enviados;
    }

    public function addEnviados(Mensaje $enviados): self
    {
        if (!$this->enviados->contains($enviados)) {
            $this->enviados[] = $enviados;
            $enviados->setIdUserA($this);
        }

        return $this;
    }

    public function removeEnviados(Mensaje $enviados): self
    {
        if ($this->enviados->contains($enviados)) {
            $this->enviados->removeElement($enviados);
            // set the owning side to null (unless already changed)
            if ($enviados->getIdUserA() === $this) {
                $enviados->setIdUserA(null);
            }
        }

        return $this;
    }

    public function addMensaje(Mensaje $mensaje): self
    {
        if (!$this->mensajes->contains($mensaje)) {
            $this->mensajes[] = $mensaje;
            $mensaje->setIdUserB($this);
        }

        return $this;
    }

    public function removeMensaje(Mensaje $mensaje): self
    {
        if ($this->mensajes->contains($mensaje)) {
            $this->mensajes->removeElement($mensaje);
            // set the owning side to null (unless already changed)
            if ($mensaje->getIdUserB() === $this) {
                $mensaje->setIdUserB(null);
            }
        }

        return $this;
    }

    public function addEnviado(Mensaje $enviado): self
    {
        if (!$this->enviados->contains($enviado)) {
            $this->enviados[] = $enviado;
            $enviado->setIdUserA($this);
        }

        return $this;
    }

    public function removeEnviado(Mensaje $enviado): self
    {
        if ($this->enviados->contains($enviado)) {
            $this->enviados->removeElement($enviado);
            // set the owning side to null (unless already changed)
            if ($enviado->getIdUserA() === $this) {
                $enviado->setIdUserA(null);
            }
        }

        return $this;
    }
}
