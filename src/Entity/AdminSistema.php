<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AdminSistemaRepository")
 */
class AdminSistema
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\User", inversedBy="adminSistema", cascade={"persist", "remove"})
     */
    private $usuarioSistema;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUsuarioSistema(): ?User
    {
        return $this->usuarioSistema;
    }

    public function setUsuarioSistema(?User $usuarioSistema): self
    {
        $this->usuarioSistema = $usuarioSistema;

        return $this;
    }
}
