<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UsuarioClientesRepository")
 */
class UsuarioClientes
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $reclamos;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $cancelados;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $devueltos;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $noRecibidos;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\User", inversedBy="usuario", cascade={"persist", "remove"})
     */
    private $Usuario;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Pedido", mappedBy="cliente")
     */
    private $pedidos;


    public function __construct()
    {
        $this->pedidos = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }


    public function getReclamos(): ?int
    {
        return $this->reclamos;
    }

    public function setReclamos(?int $reclamos): self
    {
        $this->reclamos = $reclamos;

        return $this;
    }

    public function getCancelados(): ?int
    {
        return $this->cancelados;
    }

    public function setCancelados(?int $cancelados): self
    {
        $this->cancelados = $cancelados;

        return $this;
    }

    public function getDevueltos(): ?int
    {
        return $this->devueltos;
    }

    public function setDevueltos(?int $devueltos): self
    {
        $this->devueltos = $devueltos;

        return $this;
    }

    public function getNoRecibidos(): ?int
    {
        return $this->noRecibidos;
    }

    public function setNoRecibidos(?int $noRecibidos): self
    {
        $this->noRecibidos = $noRecibidos;

        return $this;
    }

    public function getUsuario(): ?User
    {
        return $this->Usuario;
    }

    public function setUsuario(?User $Usuario): self
    {
        $this->Usuario = $Usuario;

        return $this;
    }

    /**
     * @return Collection|Pedido[]
     */
    public function getPedidos(): Collection
    {
        return $this->pedidos;
    }

    public function addPedido(Pedido $pedido): self
    {
        if (!$this->pedidos->contains($pedido)) {
            $this->pedidos[] = $pedido;
            $pedido->setCliente($this);
        }

        return $this;
    }

    public function removePedido(Pedido $pedido): self
    {
        if ($this->pedidos->contains($pedido)) {
            $this->pedidos->removeElement($pedido);
            // set the owning side to null (unless already changed)
            if ($pedido->getCliente() === $this) {
                $pedido->setCliente(null);
            }
        }

        return $this;
    }

    public function getNombre(): ?string
    {
        return $this->Usuario->getNombres();
    }


}
