<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\MaxDepth;
/**
 * @ORM\Entity(repositoryClass="App\Repository\SucursalRepository")
* normalizationContext={"groups"={"GSucursal"}
 */
class Sucursal
{
    /**
     * @ORM\Id()
     * @Groups({"Gmensaje", "ListaPedidos", "listaChats" })
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=30, nullable=true, unique=true)
     * @Groups({  "ListaPedidos", "listaChats" })
     */
    private $nombre;

    /**
     * @ORM\Column(type="string", length=200, nullable=true)
     * @Groups({"GSucursal"  })
     */
    private $direccion;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\AdminSucursal", inversedBy="sucursals")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({ "ListaPedidos", "listaChats", "GMensajesUsuario" })
     */
    private $admin;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Mensaje", mappedBy="sucursal")
     * @Groups({"GSucursal" })
     * @MaxDepth(2)
     */
    private $mensajes;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Menu", mappedBy="idSucursal")
     * @Groups({"GSucursal"})
     * @MaxDepth(2)
     */
    private $yes;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Pedido", mappedBy="sucursal")
     * @Groups({"GSucursal"})
     */
    private $pedidos;

    public function __construct()
    {
        $this->mensajes = new ArrayCollection();
        $this->yes = new ArrayCollection();
        $this->pedidos = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(?string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getDireccion(): ?string
    {
        return $this->direccion;
    }

    public function setDireccion(?string $direccion): self
    {
        $this->direccion = $direccion;

        return $this;
    }

    public function getAdmin(): ?AdminSucursal
    {
        return $this->admin;
    }

    public function setAdmin(?AdminSucursal $admin): self
    {
        $this->admin = $admin;

        return $this;
    }

    /**
     * @return Collection|Mensaje[]
     */
    public function getMensajes(): Collection
    {
        return $this->mensajes;
    }

    public function addMensaje(Mensaje $mensaje): self
    {
        if (!$this->mensajes->contains($mensaje)) {
            $this->mensajes[] = $mensaje;
            $mensaje->setSucursal($this);
        }

        return $this;
    }

    public function removeMensaje(Mensaje $mensaje): self
    {
        if ($this->mensajes->contains($mensaje)) {
            $this->mensajes->removeElement($mensaje);
            // set the owning side to null (unless already changed)
            if ($mensaje->getSucursal() === $this) {
                $mensaje->setSucursal(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Menu[]
     */
    public function getYes(): Collection
    {
        return $this->yes;
    }

    public function addYe(Menu $ye): self
    {
        if (!$this->yes->contains($ye)) {
            $this->yes[] = $ye;
            $ye->setIdSucursal($this);
        }

        return $this;
    }

    public function removeYe(Menu $ye): self
    {
        if ($this->yes->contains($ye)) {
            $this->yes->removeElement($ye);
            // set the owning side to null (unless already changed)
            if ($ye->getIdSucursal() === $this) {
                $ye->setIdSucursal(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Pedido[]
     */
    public function getPedidos(): Collection
    {
        return $this->pedidos;
    }

    public function addPedido(Pedido $pedido): self
    {
        if (!$this->pedidos->contains($pedido)) {
            $this->pedidos[] = $pedido;
            $pedido->setSucursal($this);
        }

        return $this;
    }

    public function removePedido(Pedido $pedido): self
    {
        if ($this->pedidos->contains($pedido)) {
            $this->pedidos->removeElement($pedido);
            // set the owning side to null (unless already changed)
            if ($pedido->getSucursal() === $this) {
                $pedido->setSucursal(null);
            }
        }

        return $this;
    }
}
