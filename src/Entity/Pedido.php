<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PedidoRepository")
 * @ORM\Entity(repositoryClass="App\Repository\UsuarioClientesRepository")
  * normalizationContext={"groups"={ "productosPedido"}}
  * denormalizationContext={"groups"={"productosPedido", "ListaPedidos"}}
 */
class Pedido
{


    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({ "ListaPedidos" })
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     * @Groups({ "productosPedido", "ListaPedidos" })
     */
    private $fecha;

    /**
     * @ORM\Column(type="float", nullable=true)
     * @Groups({ "productosPedido", "ListaPedidos" })
     */
    private $total;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\UsuarioClientes", inversedBy="pedidos")
     * @ORM\JoinColumn(nullable=false)
     */
    private $cliente;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Sucursal", inversedBy="pedidos")
     * @Groups({ "ListaPedidos" })
     * @ORM\JoinColumn(nullable=false)
     */
    private $sucursal;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Estado")
     * @Groups({ "ListaPedidos" })
     * @ORM\JoinColumn(nullable=false)
     */
    private $estado;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\PedidoProducto", mappedBy="pedido", cascade={"persist"})
     * @Groups({ "productosPedido" })
     */
    private $productos;

    public function __construct()
    {
        $this->productos = new ArrayCollection();
    }



    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFecha(): ?\DateTimeInterface
    {
        return $this->fecha;
    }

    public function setFecha(\DateTimeInterface $fecha): self
    {
        $this->fecha = $fecha;

        return $this;
    }

    public function getTotal(): ?float
    {
        return $this->total;
    }

    public function setTotal(?float $total): self
    {
        $this->total = $total;

        return $this;
    }

    public function getCliente(): ?UsuarioClientes
    {
        return $this->cliente;
    }

    public function setCliente(?UsuarioClientes $cliente): self
    {
        $this->cliente = $cliente;

        return $this;
    }

    //public function setClienteID(?int $cliente, UsuarioClientesRepository $SucursalRepository): self
    //{
    //    $this->cliente = $SucursalRepository->findById($cliente);
    //    return $this;
    //}

    public function getSucursal(): ?Sucursal
    {
        return $this->sucursal;
    }

    public function setSucursal(?Sucursal $sucursal): self
    {
        $this->sucursal = $sucursal;

        return $this;
    }

    public function getEstado(): ?estado
    {
        return $this->estado;
    }

    public function setEstado(?estado $estado): self
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * @return Collection|PedidoProducto[]
     */
    public function getProductos(): Collection
    {
        return $this->productos;
    }

    public function addProductos(PedidoProducto $productos): self
    {
        if (!$this->productos->contains($productos)) {
            $this->productos[] = $productos;
            $productos->setPedido($this);
        }

        return $this;
    }

    public function removeProducto(PedidoProducto $productos): self
    {
        if ($this->productos->contains($productos)) {
            $this->productos->removeElement($productos);
            // set the owning side to null (unless already changed)
            if ($productos->getPedido() === $this) {
                $productos->setPedido(null);
            }
        }

        return $this;
    }

    public function addProducto(PedidoProducto $producto): self
    {
        if (!$this->productos->contains($producto)) {
            $this->productos[] = $producto;
            $producto->setPedido($this);
        }

        return $this;
    }


}
