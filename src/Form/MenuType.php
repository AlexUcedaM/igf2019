<?php

namespace App\Form;

use App\Entity\Menu;
use App\Entity\Sucursal;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class MenuType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombre', TextType::class, array( 'required' => true, 'attr'=> array('class' => 'form-control','placeholder' => 'nombre?')))
            ->add('descripcion', TextareaType::class, array('required' => true,'attr'=> array('name2' => 'Descripción','class' => 'form-control','placeholder' => 'Descripción')))

        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Menu::class,
        ]);
    }
}
