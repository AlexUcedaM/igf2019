<?php

namespace App\Form;

use App\Entity\Pedido;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class PedidoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('fecha')
            ->add('total')
            ->add('sucursal', EntityType::class, ['class' => 'App:Sucursal','choice_label' => 'nombre','attr'=> array('class' => 'form-control',)])
            ->add('estado', EntityType::class, ['class' => 'App:Estado','choice_label' => 'nombre','attr'=> array('class' => 'form-control',)])
            ->add('cliente', EntityType::class, ['class' => 'App:UsuarioClientes','choice_label' => 'nombre','attr'=> array('class' => 'form-control',)])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Pedido::class,
        ]);
    }
}
