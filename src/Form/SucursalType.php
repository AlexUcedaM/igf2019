<?php

namespace App\Form;

use App\Entity\Sucursal;
use App\Entity\AdminSucursal;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
class SucursalType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder

            ->add('nombre', TextType::class, array( 'required' => true, 'attr'=> array('class' => 'form-control','placeholder' => 'nombre?')))
            ->add('direccion', TextType::class, array( 'required' => true, 'attr'=> array('class' => 'form-control','placeholder' => 'dirección?')))
            ->add('Admin', EntityType::class, ['class' => 'App:AdminSucursal','choice_label' => 'usuario','attr'=> array('class' => 'form-control',)])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Sucursal::class,
            'attr' => [
               'id'    => 'sucursal-id',
               'name'  => 'form-name',
               'class' => 'class-name'
             ],
              
        ]);
    }
}
