<?php

namespace App\Form;

use App\Entity\Producto;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Vich\UploaderBundle\Form\Type\VichFileType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\FileType;


class ProductoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombreProducto', TextType::class, array( 'required' => true, 'block_name' => 'nameejempl', 'attr'=> array('class' => 'form-control','placeholder' => 'Nombre del producto')))
            //->add('cantidad',IntegerType::class ,  array('label' => 'Existencias', 'attr'=>array( 'name'  => 'form-name', 'name2' => 'nams666ad','required' => true, 'placeholder' => 'Cantidad en existencia', 'class' => 'form-control')))
            //->add('SKU', TextType::class, array('required' => true,'attr'=> array('name2' => 'nam222sad','class' => 'form-control','placeholder' => 'SKU')))
            ->add('precioCompra', MoneyType::class, ['required'=> true, 'currency' => 'USD', 'label' =>  'Precio de compra', 'attr'=> array('class' => 'form-control', 'placeholder'=>'#.##', 'pattern'=>'\d+(\.\d{1,2})?')])
            ->add('precioVenta', MoneyType::class, ['required'=> true, 'currency' => 'USD', 'label' =>  'Precio', 'attr'=> array('class' => 'form-control', 'placeholder'=>'#.##', 'pattern'=>'\d+(\.\d{1,2})?'  )])
            ->add('image', TextType::class, array('required' => true,'attr'=> array('class' => 'form-control','placeholder' => 'Imagen')))
            ->add('imagen', TextType::class, array('required' => true,'attr'=> array('class' => 'form-control','placeholder' => 'Imagen')))
            ->add('descripcion', TextareaType::class, array('required' => true,'attr'=> array('name2' => 'Descripción','class' => 'form-control','placeholder' => 'Descripción')))
            ->add('imageFile', VichFileType::class, array(
                'required'      => false,
                'allow_delete'  => true, // not mandatory, default is true
                'download_link' => true, // not mandatory, default is true
                'attr'=> array('name2' => 'namsddad','class' => 'form-control','placeholder' => 'Archivo de imagen'),'attr'=> array('class' => 'form-control','placeholder' => 'Seleccione una imagen')
            ))
            ->add('menu', EntityType::class, ['class' => 'App:Menu','choice_label' => 'nombre','attr'=> array('required' => true,'class' => 'form-control',)])

        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Producto::class,
            'attr' => [
               'id'    => 'producto-id',
               'name'  => 'form-name',
               'class' => 'class-name'
             ],

        ]);
    }
}
