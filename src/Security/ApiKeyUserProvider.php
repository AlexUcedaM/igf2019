<?php
namespace App\Security;

use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\User\User;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\User as userE;
use FOS\UserBundle\Model\UserInterface as userI;
use FOS\UserBundle\Model\UserManagerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class ApiKeyUserProvider implements UserProviderInterface
{

    private $userManager;
    private $passwordEncoder;
    private $roles;
    public function __construct( UserManagerInterface $userManager, UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->userManager = $userManager;
        $this->passwordEncoder = $passwordEncoder;
    }
    public function getUsernameForApiKey($apiKey)
    {
        $username="";
        list($identificador, $pass) = explode(":", $apiKey);
        $user = $this->userManager->findUserByUsername($identificador);

        // Look up the username based on the token in the database, via
        // an API call, or do something entirely different
        //$p= password_hash($pass, PASSWORD_DEFAULT);
        //echo "1";
        $bool = $this->passwordEncoder->isPasswordValid($user, $pass);
        $this->roles= $user->getRoles();
        if($bool)
        $username = $user->getId();
        return $username;
    }

    public function loadUserByUsername($username)
    {
        //echo "8";
        return new User(
            $username,
            null,
            $this->roles
        );
    }
    public function refreshUser(UserInterface $user)
    {
        // this is used for storing authentication in the session
        // but in this example, the token is sent in each request,
        // so authentication can be stateless. Throwing this exception
        // is proper to make things stateless
        throw new UnsupportedUserException();
    }

    public function supportsClass($class)
    {
        return User::class === $class;
        //echo "-10";
    }
}