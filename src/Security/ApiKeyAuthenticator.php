<?php

namespace App\Security;

use App\Security\ApiKeyUserProvider;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\PreAuthenticatedToken;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Http\Authentication\SimplePreAuthenticatorInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationFailureHandlerInterface;
use Symfony\Component\HttpFoundation\Response;
use FOS\UserBundle\Model\UserManagerInterface;


class ApiKeyAuthenticator implements SimplePreAuthenticatorInterface, AuthenticationFailureHandlerInterface
{
    private $p="";
    private $userManager;
    
    public function __construct( UserManagerInterface $userManager)
    {
        $this->userManager = $userManager;
    }

    public function createToken(Request $request, $providerKey)
    {
        


        $apiKey=null;$identificador=null; $pass=null;
        $apiKey = $request->query->get('apikey');
        if( is_null($apiKey) ){
            throw new CustomUserMessageAuthenticationException(
                sprintf('{ "login" :  "false" }')  // NO SE HA LOGUEADO Y NO MANDO APIKEY
            );
            return null;
        }
        
        list($identificador, $pass) = explode(":", $apiKey);
        $user = $this->userManager->findUserByUsername($identificador);
        try {
            $id = (string) $user->getId();
        } catch(\Throwable $e) {
            throw new CustomUserMessageAuthenticationException(
                sprintf('{ "login" :  "false" }')  //EL USUARIO NO ESTA EN LA BASE DE DATOS
            );
        }

        ////echo "-2";
        return new PreAuthenticatedToken( $identificador, $apiKey, $providerKey);
    }
    
    public function authenticateToken(TokenInterface $token, UserProviderInterface $userProvider, $providerKey)
    {
        if (!$userProvider instanceof ApiKeyUserProvider) {
            throw new CustomUserMessageAuthenticationException(
                sprintf('{ "login" :  "false" }')  //EL USUARIO NO ESTA EN LA BASE DE DATOS
            );
        }

        $apiKey = $token->getCredentials();
        $username = $userProvider->getUsernameForApiKey($apiKey);

        if (!$username) {
            throw new CustomUserMessageAuthenticationException(
                sprintf('{ "login" :  "false" }')  //EL USUARIO NO ESTA EN LA BASE DE DATOS
            );
        }

        $user = $userProvider->loadUserByUsername($username);

        return new PreAuthenticatedToken(
            $user,
            $apiKey,
            $providerKey,
            $user->getRoles()
        );
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        //echo "-4";
        return new Response(
            '{ "login" :  "false" }'
        );
    }

    public function supportsToken(TokenInterface $token, $providerKey)
    {
        //echo "-3";
        return $token instanceof PreAuthenticatedToken && $token->getProviderKey() === $providerKey;
    }
}