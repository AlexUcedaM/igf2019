var st;
$(document).ready(function() {

  var p1= document.getElementById('contenidoMensajes');
  if(p1 !=null){
    p1.style.display = 'block';
  }
  var p2= document.getElementById('contenidoPedidos');
  if(p2 !=null){
    p2.style.display = 'none';
  }
  var p3= document.getElementById('contenidoPlatillosProductos');
  if(p3 !=null){
    p3.style.display = 'none';
  }
  var p4= document.getElementById('contenidoSucursales');
  if(p4 !=null){
    p4.style.display = 'none';
  }
  var p5= document.getElementById('contenidoGraficosAdmin');
  if(p5 !=null){
    p5.style.display = 'none';
  }
  var p6= document.getElementById('contenidoReportes');
  if(p6 !=null){
      p6.style.display = 'none';
  }

  var authElements =
  "{\"userData\":\""+idUsuario+"\",\"tipo\":\"1\",\"destino\":\""+document.getElementById("txt_enviar").value+"\",\"mensaje\":\""+document.getElementById("txt_enviar").value+"\"}";
  var  notif = new WebSocket("ws://127.0.0.1:8080");
  var destinatario;

  function cargarMensajes(sucursal, emisor){
    $.ajax({
      url:"/pintarMensajes",
      data: {
         'sucursal': sucursal,
         'emisor': emisor,
      },
      type:'POST',
      error:function(err){
      },
      success:function(data){
          $(mensajesContenidos).html("");
          $("#mensajesContenidos").html(data);
      },
      complete:function(){
      }
    })
  }


    function NotifServer(){
       notif.onmessage = function (event) {
          //alertify.success(event.data);
           st= event.data;
           var obj = JSON.parse(st);
           //alert(obj.origen);
          if(obj.origen == destinatario ){
            printRequest(obj.mensaje);
          }else{
            Toast.fire({ icon:'success', title: "Mensaje nuevo de: "+ obj.nombre})
            //printRequest(obj.mensaje);
          }

        }
        notif.onopen = function() {
          $('.content').append('<p> >>> Connected</p>');
          var authElements ="{\"token\":\""+getToken()+"\",\"userData\":\""+idUsuario+"\",\"tipo\":\"1\",\"destino\":\""+destinatario+"\",\"mensaje\":\""+document.getElementById("txt_enviar").value+"\"}";
          notif.send(authElements);
        }
        notif.onerror = function(error) {
          //alert('error');
        }
    }

      NotifServer();

      $('body').on('click', '#chats a', function(){
        destinatario = this.name;

        $(destinatario).html("");
        $("#destinatario").html(document.getElementById(this.id).dataset.nombre);
        sucursal= document.getElementById(this.id).dataset.id;
        cargarMensajes(sucursal, this.name);

      });

      $('#action_menu_btn').click(function(){
        $('.action_menu').toggle();
      });

      $('#btn_enviar').click(function(){
        if(idUsuario== null | destinatario==null | document.getElementById("txt_enviar").value=='' ){
          Toast.fire({ icon:'error', title: "Falta un parametro"})
        }else{
          authElements =
          "{\"token\":\""+getToken()+"\",\"userData\":\""+idUsuario+"\",\"tipo\":\"2\",\"destino\":\""+destinatario+"\",\"sucursal\":\""+sucursal+"\",\"mensaje\":\""+document.getElementById("txt_enviar").value+"\"}";
          notif.send(authElements);
          
          printMessage( document.getElementById("txt_enviar").value );
        }



      });

      function printRequest(respuesta){
        $("#mensajesContenidos").append(
          "<div class='d-flex justify-content-start mb-4'><div class='img_cont_msg'>  <img src='https://static.turbosquid.com/Preview/001292/481/WV/_D.jpg' class='rounded-circle user_img_msg'></div><div class='msg_cotainer'>"+respuesta+"<span class='msg_time'>8:40 AM, Today</span></div></div>"
        );
        var barraMensajes=document.getElementById("mensajesContenidos");
        barraMensajes.scrollTop = barraMensajes.scrollHeight;
      }

      function printMessage(mensaje){
        $("#mensajesContenidos").append(
        "<div class='d-flex justify-content-end mb-4'><div class='msg_cotainer_send'>"+
          mensaje
          +" <span class='msg_time_send'>8:55 AM, Today</span></div><div class='img_cont_msg'></div></div>"
        );

        var barraMensajes=document.getElementById("mensajesContenidos");
        barraMensajes.scrollTop = barraMensajes.scrollHeight;
      }

      function getToken(){
        var token;
        $.ajax({
            url:"/socketval/pedir_token",
            async: false,
            type:'POST',
            data: {
            },
            error:function(err){
                  console.error(err);
            },
            success:function(data){
                var st= Object.values(data);
                token= data.data;
            },
            complete:function(){
            }
        });
        return token;
      }

        $('body').on('click', '#sucursalLista a', function(){ //Usuario selecciona una sucursal
          event.stopPropagation();
          event.stopImmediatePropagation();
          
                notif.close();     
        });
    });
