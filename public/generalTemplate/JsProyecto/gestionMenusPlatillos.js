$(document).ready(function() {

  var p1= document.getElementById('contenidoMensajes');
  if(p1 !=null){
    p1.style.display = 'none';
  }
  var p2= document.getElementById('contenidoPedidos');
  if(p2 !=null){
    p2.style.display = 'none';
  }
  var p3= document.getElementById('contenidoPlatillosProductos');
  if(p3 !=null){
    p3.style.display = 'block';
  }
  var p4= document.getElementById('contenidoSucursales');
  if(p4 !=null){
    p4.style.display = 'none';
  }
  var p5= document.getElementById('contenidoGraficosAdmin');
  if(p5 !=null){
    p5.style.display = 'none';
  }
  var p6= document.getElementById('contenidoReportes');
  if(p6 !=null ){
      p6.style.display = 'none';
  }

  cargarMenus();
  comboMenus();
  $('body').on('click', '#sucursalLista a', function(){

        cargarMenus();
        comboMenus();
    });
  $('body').on('click', '#productos i', function(){
          event.stopPropagation();
          event.stopImmediatePropagation();
          idProducto=document.getElementById(this.id).dataset.id;

          opt1="imgProducto"+idProducto;
          opt2="eliminarProducto"+idProducto;
          cadena=this.id;
          if(cadena.includes('editProducto')){
            rellenarFormProducto();
            imagen=document.getElementById("imgProducto"+idProducto);
            $("#imgProductoSeleccionado").html("");
            var clon = imagen.cloneNode(true);
            $("#imgProductoSeleccionado").html(clon);

            document.getElementById("guardar").disabled= true;
            document.getElementById("btnEditarProducto").disabled= false;
            document.getElementById("btnEliminarProducto").disabled= false;

          }

          if(cadena.includes('eliminarProducto')){
            Swal.fire({
              title: 'Desea eliminar?',
              text: "ELIMINAR! ?",
              icon: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#d33',
              cancelButtonColor: '#3085d6',
              confirmButtonText: 'Si borrar!'
            }).then((result) => {
              if (result.value) {
                eliminarProducto();
                Swal.fire(
                  'Deleted!',
                  'Your file has been deleted.',
                  'success'
                )
              }
            })

          }
      });
  $('body').on('click', '#TablaMenus button', function(){
        event.stopPropagation();
        event.stopImmediatePropagation();
        idMenu=document.getElementById(this.id).dataset.idmenu;
        var nombre=document.getElementById("menuNombre"+idMenu).textContent;
        var descripcion=document.getElementById("menuDescripcion"+idMenu).textContent;
        ////alert(nombre);
        document.getElementById("menu_nombre").value= nombre;
        document.getElementById("menu_descripcion").value = descripcion;
        document.getElementById("btnGuardarMenu").disabled= true;
        document.getElementById("btnEliminarMenu").disabled= false;
        document.getElementById("btnEditarMenu").disabled= false;
      });


      function verificarNombreMenu(){
      var retorno;
        $.ajax({
          url:"/menu/verificarNombreMenu",
          type:'POST',
          async: false,
          data: {
             'nombre': $("#menu_nombre").val(),
             'sucursal': sucursal,
          },
          error:function(err){
                //console.error(err);
          },
          success:function(data){
              var st= Object.values(data);
              if(st[0]=='repetido'){
                Toast.fire({ icon:'success', title: "Nombre repetido"})
                ////alertify.error(st[0]);
                retorno=0;
              }else{
                retorno=1;
              }
          },
          complete:function(){
          }
        })
        return retorno;
      };
async function pintarUnProducto(idProducto){
    await $.ajax({
        url: "/producto/pintar_uno_ajax",
        data: {
           'id': idProducto,
        },
        type: 'POST',
        error: function(err) {
          Toast.fire({ icon:'success', title: err })
        },
        success: function(data) {

          $("#productos").append(data);
        },
        complete: function() {}
    })
}
      function fLimpiarProducto(){
  idProducto=null;
  document.getElementById("producto_nombreProducto").value="";
  document.getElementById("producto_descripcion").value="";
  document.getElementById("producto_precioCompra").value="";
  document.getElementById("producto_precioVenta").value="";
  document.getElementById("producto_imagen").value="";
  document.getElementById("producto_menu").value="";
  document.getElementById("guardar").disabled= false;
  document.getElementById("btnEditarProducto").disabled= true;
  document.getElementById("btnEliminarProducto").disabled= true;
  $("#imgProductoSeleccionado").html("");
}
async function verificarNombreProducto(){
      await $.ajax({
        url:"/producto/verificarNombre",
        type:'POST',
        data: {
           'producto': $("#producto_nombreProducto").val(),
        },
        error:function(err){
              //console.error(err);
        },
        success:function(data){
            var st= Object.values(data);

            if(st[0]=='repetido'){
              verificarProducto=false;
              Toast.fire({ icon: 'success',title: 'Nombre repetido'})
            }else{
              verificarProducto=true;
              //document.getElementById("guardar").disabled = false;
            }
        },
        complete:function(){
        }
      });

};
      function comboMenus(){
         $.ajax({
           url: "/menu/combo_menu_ajax",
           type: 'POST',
           data: {
             'sucursal': sucursal,
           },
           success: function(data){
             $("#producto_menu").empty();
             $("#filtrar_producto_menu").empty();

             $("#producto_menu").append('<option value="0"> Select un menu...</option>');
             $("#filtrar_producto_menu").append('<option value="0"> Select un menu...</option>');
             for ( i = 0 ; i < data.data.length; i++){
               var nuevafila = '<option value="'+data.data[i].ids+'">'+data.data[i].nombre+"</option>";
               $("#producto_menu").append(nuevafila);
               $("#filtrar_producto_menu").append(nuevafila);
             }
           },
           fail: function() { //alert("error");
           },
           complete: function () {}
         })
         .done(function(data){
         })
         .fail(function(){})
         .always(function(){});
       }

      function cargarMenus(){
        $.ajax({
            url: "/producto/cargar_menu_ajax",
            data: {
               'sucursal': sucursal,
            },
            type: 'POST',
            error: function(err) {
                //console.error(err);
            },
            success: function(data) {
              $("#TablaMenus").html("");
              $("#TablaMenus").append(data);
            },
            complete: function() {}
        })
      };

async function eliminarProducto(){
  await $.ajax({
      url: "/producto/eliminar_producto_ajax",
      data: {
         'id': idProducto,
      },
      type: 'POST',
      error: function(err) {
          //console.error(err);
      },
      success: function(data) {
        st= Object.values(data);
      },
      complete: function() {
        var d="#divProducto"+idProducto;
        $(d).remove();
      }
  });

  idProducto=null;
  return "algo";
}
async function pintarUnProducto(idProducto){
    await $.ajax({
        url: "/producto/pintar_uno_ajax",
        data: {
           'id': idProducto,
        },
        type: 'POST',
        error: function(err) {
          Toast.fire({ icon:'success', title: err })
        },
        success: function(data) {

          $("#productos").append(data);
        },
        complete: function() {}
    })
}
      function rellenarFormProducto() {
  var st;
     $.ajax({
        url:"/producto/json_producto",
        data:{
           'id': idProducto,
        },
        type:'POST',

        async: false,
        error:function(err){
              //console.error(err);
        },
        success:function(data){
            st= Object.values(data);
        },
        complete:function(){
        }
      });
      //document.getElementById("producto_imageFile_file").value= ;
    document.getElementById("producto_nombreProducto").value= st[0] ;
    document.getElementById("producto_descripcion").value= st[1];
    document.getElementById("producto_precioCompra").value= st[2];
    document.getElementById("producto_precioVenta").value= st[3];
    document.getElementById("producto_imagen").value= st[4];
    document.getElementById("producto_menu").value= st[5];
    document.getElementById("producto_imageFile_file").value="";

};
      function iniciarBotones(){
  document.getElementById("guardar").disabled= false;
  document.getElementById("btnEditarProducto").disabled= true;
  document.getElementById("btnEliminarProducto").disabled= true;
}

$("#btnEditarProducto").click( async function () {
      var formData = new FormData();
      formData.append("upload[file]", document.getElementById("producto_imageFile_file").files[0]);
      formData.append("upload[nombreProducto]", document.getElementById("producto_nombreProducto").value);
      formData.append("upload[descripcion]", document.getElementById("producto_descripcion").value);
      formData.append("upload[precioCompra]", document.getElementById("producto_precioCompra").value);
      formData.append("upload[precioVenta]", document.getElementById("producto_precioVenta").value);
      formData.append("upload[imagen]", document.getElementById("producto_imagen").value);
      formData.append("upload[menu]", document.getElementById("producto_menu").value);
      formData.append("upload[idProducto]", idProducto);
      var st;

      await $.ajax({
          url:"/producto/editar_producto_Ajax",
          data: formData,
          type:'POST',
          contentType:false,
          processData:false,
          cache:false,
          async: false,
          error:function(err){
                //console.error(err);
          },
          success:function(data){
              st= Object.values(data);
          },
          complete:function(){
          }
        });

        if(st[0]="true"){
          var d="#divProducto"+idProducto;
          $(d).remove();

          $("#imgProductoSeleccionado").html("");
          var clon = imagen.cloneNode(true);
          $("#imgProductoSeleccionado").html(clon);

            Toast.fire({ icon:'success', title: "Listo!"})
              if(menu_filtro == st[2] ){
                pintarUnProducto(st[1]);
              }
        }


});
$('#btnLimpiarProducto').click(function(){
  fLimpiarProducto();
});


$("#guardar").click( async function () {
   await verificarNombreProducto();
   var form=document.getElementById("producto-id")

   if(form.checkValidity()){
     if(document.getElementById("producto_menu").value>0){
       if(verificarProducto){
         var formData = new FormData();
         formData.append("upload[file]", document.getElementById("producto_imageFile_file").files[0]);
         formData.append("upload[nombreProducto]", document.getElementById("producto_nombreProducto").value);
         formData.append("upload[descripcion]", document.getElementById("producto_descripcion").value);
         formData.append("upload[precioCompra]", document.getElementById("producto_precioCompra").value);
         formData.append("upload[precioVenta]", document.getElementById("producto_precioVenta").value);
         formData.append("upload[imagen]", document.getElementById("producto_imagen").value);
         formData.append("upload[menu]", document.getElementById("producto_menu").value);
         var st;

         await $.ajax({
             url:"/producto/crearAjax",
             data: formData,
             type:'POST',
             contentType:false,
             processData:false,
             cache:false,
             async: false,
             error:function(err){
                   //console.error(err);
             },
             success:function(data){
                 st= Object.values(data);
             },
             complete:function(){
             }
           });
           if(st[0]="true"){
               Toast.fire({ icon:'success', title: "Listo!"})
                 if(menu_filtro == st[2] ){
                   pintarUnProducto(st[1]);
                 }
           }
         }else {
           Toast.fire({ icon:'error', title: "Nombre repetido"})
         }
     }else {
       Toast.fire({ icon:'error', title: "Seleccione menu"})
     }
   }else{
     form.reportValidity();
   }


});
$('#btnFiltrarProductos').click(function(){
    menu_filtro=document.getElementById("filtrar_producto_menu").value;
    $.ajax({
        url: "/producto/filtrar_productos_ajax",
        data: {
           'menu': menu_filtro,

        },
        type: 'POST',
        error: function(err) {
            //console.error(err);
        },
        success: function(data) {

          var st= Object.values(data);
          $("#productos").html("");
          $("#productos").html(data);

          if(st[0]=='aaaa'){
            ////alertify.set('notifier','position','bottom-left');
            ////alertify.error("Sin resultados");
          }else{
            ////alertify.error("Listo");
          }
        },
        complete: function() {}
    })

});
$('#btnEliminarMenu').click(function(){
  $.ajax({
    url: "/menu/eliminar_menu_ajax",
    data: {
        'menu': idMenu,
    },
    type: 'POST',
    error: function(err) {
    },
    success: function(data) {
      cargarMenus();
      var st= Object.values(data);
      comboMenus();
    },
    complete: function() {}
    })
});
$('#btnEditarMenu').click(function(){
    if(verificarNombreMenu()==1){
      $.ajax({
          url: "/menu/editar_menu_ajax",
          data: {
             'menu': idMenu,
             'nombre': $("#menu_nombre").val(),
             'descripcion': $("#menu_descripcion").val(),
          },
          type: 'POST',
          error: function(err) {
              //console.error(err);
          },
          success: function(data) {
            cargarMenus();
            var st= Object.values(data);
            comboMenus();
            Toast.fire({ icon:'success', title: "Listo!" })
          },
          complete: function() {}
      })
    }
});
$('#btnLimpiarMenu').click(function(){
  idMenu=0;
  document.getElementById("menu_nombre").value= "";
  document.getElementById("menu_descripcion").value = "";
  document.getElementById("btnGuardarMenu").disabled= false;
  document.getElementById("btnEliminarMenu").disabled= true;
  document.getElementById("btnEditarMenu").disabled= true;
});
$('#btnGuardarMenu').click(function(){
    if(verificarNombreMenu()==1){
      $.ajax({
          url: "/menu/crear_menu_ajax",
          data: {
             'sucursal': sucursal,
             'nombre': $("#menu_nombre").val(),
             'descripcion': $("#menu_descripcion").val(),
          },
          type: 'POST',
          error: function(err) {
              //console.error(err);
          },
          success: function(data) {
            cargarMenus();
            var st= Object.values(data);
            comboMenus();
            ////alert(st[0]);
          },
          complete: function() {}
      })
    }

});
$('#btnbuscar').click(function(){
    $.ajax({
      url:"/producto/buscarAjax",
      type:'POST',
      data: {
         'frase': $("#txtbuscar").val(),
      },
      error:function(err){
            //console.error(err);
      },
      success:function(data){
          var st= Object.values(data);
          $("#productos").html("  ");
          $("#productos").html(data);

          if(st[0]=='aaaa'){
            ////alertify.set('notifier','position','bottom-left');
            ////alertify.error("Sin resultados");
          }else{
            ////alertify.error("Listo");
          }
      },
      complete:function(){
        }
      })
});
$('#btnEliminarProducto').click(async function(){
    await eliminarProducto();
    idProducto=null;
});

$('body').on('click', '#TablaMenus button', function(){
  event.stopPropagation();
  event.stopImmediatePropagation();
  idMenu=document.getElementById(this.id).dataset.idmenu;
  var nombre=document.getElementById("menuNombre"+idMenu).textContent;
  var descripcion=document.getElementById("menuDescripcion"+idMenu).textContent;
  ////alert(nombre);
  document.getElementById("menu_nombre").value= nombre;
  document.getElementById("menu_descripcion").value = descripcion;
  document.getElementById("btnGuardarMenu").disabled= true;
  document.getElementById("btnEliminarMenu").disabled= false;
  document.getElementById("btnEditarMenu").disabled= false;
});

});
