$(document).ready(function() {

    cargarSucursales();

        var p1= document.getElementById('contenidoMensajes');
        if(p1 !=null){
            p1.style.display = 'none';
        }
        var p2= document.getElementById('contenidoPedidos');
        if(p2 !=null){
            p2.style.display = 'none';
        }
        var p3= document.getElementById('contenidoPlatillosProductos');
        if(p3 !=null){
            p3.style.display = 'none';
        }
        var p4= document.getElementById('contenidoSucursales');
        if(p4 !=null){
            p4.style.display = 'block';
        }
        var p5= document.getElementById('contenidoGraficosAdmin');
        if(p5 !=null){
            p5.style.display = 'none';
        }
        var p6= document.getElementById('contenidoReportes');
        if(p6 !=null ){
            p6.style.display = 'none';
        }

});


$("#btnLimpiarSucursal").click( async function () {
    document.getElementById("sucursal_nombre").value = null;
    document.getElementById("sucursal_direccion").value = null;
    document.getElementById("sucursal_Admin").value = null;
    idSucursal  = null;
});

$("#btnEditarSucursal").click( async function () {
//await verificarNombreSucursal();
var form=document.getElementById("sucursal-id")
    if(form.checkValidity()){
        if(document.getElementById("sucursal_Admin").value>0){
                var formData = new FormData();
                formData.append("upload[sucursal_nombre]", document.getElementById("sucursal_nombre").value);
                formData.append("upload[sucursal_direccion]", document.getElementById("sucursal_direccion").value);
                formData.append("upload[sucursal_Admin]", document.getElementById("sucursal_Admin").value);
                formData.append("upload[id]", idSucursal );
                var st;

            await $.ajax({
                    url:"/sucursal/editarAjax",
                    data: formData,
                    type:'POST',
                    contentType:false,
                    processData:false,
                    cache:false,
                    async: false,
                    error:function(err){
                        //console.error(err);
                    },
                    success:function(data){
                        st= Object.values(data);
                    },
                    complete:function(){
                    }
            });
                if(st[0]="true"){
                    Toast.fire({ icon:'success', title: "Listo!"})
                        cargarSucursales();
                }
        }else {
            Toast.fire({ icon:'error', title: "Seleccione admin"})
        }
    }else{
        form.reportValidity();
    }
});




$('body').on('click', '#TablaSucursals button', function(){

    Swal.fire({
        title: 'PRECAUCION?',
        text: "No debe editar el admin de sucursales que ya contengan chats",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'entendido!'
        }).then((result) => {
        if (result.value) {
           event.stopPropagation();
            event.stopImmediatePropagation();
            idSucursal=document.getElementById(this.id).dataset.idsucursal;
            var nombre=document.getElementById("sucursalNombre"+idSucursal).textContent;
            var direccion=document.getElementById("sucursalDireccion"+idSucursal).textContent;
            var adminSucursal=document.getElementById(this.id).dataset.admin;
            ////alert(nombre);
            document.getElementById("sucursal_nombre").value= nombre;
            document.getElementById("sucursal_direccion").value = direccion;
            document.getElementById("sucursal_Admin").value = adminSucursal;
        }
    })
    
});

$("#btnActualizarTabla").click(function(){
    cargarSucursales();
});

function cargarSucursales(){
    $.ajax({
        url: "/sucursal/cargarTabla",
        data: {
        
        },
        type: 'POST',
        error: function(err) {
            //console.error(err);
        },
        success: function(data) {
        $("#TablaSucursals").html("");
        $("#TablaSucursals").append(data);
        },
        complete: function() {}
    })
};

$("#btnGuardarSucursal").click( async function () {
//await verificarNombreSucursal();
var form=document.getElementById("sucursal-id")
    if(form.checkValidity()){
        if(document.getElementById("sucursal_Admin").value>0){
                var formData = new FormData();
                formData.append("upload[sucursal_nombre]", document.getElementById("sucursal_nombre").value);
                formData.append("upload[sucursal_direccion]", document.getElementById("sucursal_direccion").value);
                formData.append("upload[sucursal_Admin]", document.getElementById("sucursal_Admin").value);
                var st;

            await $.ajax({
                    url:"/sucursal/crearAjax",
                    data: formData,
                    type:'POST',
                    contentType:false,
                    processData:false,
                    cache:false,
                    async: false,
                    error:function(err){
                        //console.error(err);
                    },
                    success:function(data){
                        st= Object.values(data);
                    },
                    complete:function(){
                    }
            });
                if(st[0]="true"){
                    Toast.fire({ icon:'success', title: "Listo!"})
                        if(menu_filtro == st[2] ){
                        pintarUnSucursal(st[1]);
                        }
                }
        }else {
            Toast.fire({ icon:'error', title: "Seleccione admin"})
        }
    }else{
        form.reportValidity();
    }
});
