$(document).ready(function() {

  var p1= document.getElementById('contenidoMensajes');
  if(p1 !=null){
    p1.style.display = 'none';
  }
  var p2= document.getElementById('contenidoPedidos');
  if(p2 !=null){
    p2.style.display = 'block';
  }
  var p3= document.getElementById('contenidoPlatillosProductos');
  if(p3 !=null){
    p3.style.display = 'none';
  }
  var p4= document.getElementById('contenidoSucursales');
  if(p4 !=null){
    p4.style.display = 'none';
  }
  var p5= document.getElementById('contenidoGraficosAdmin');
  if(p5 !=null){
    p5.style.display = 'none';
  }
  var p6= document.getElementById('contenidoReportes');
  if(p6 !=null && p!= enfocar ){
      p6.style.display = 'none';
  }

  $('body').on('click', '#sucursalLista a', function(){
  filtrarPedidos();

  });
  $('body').on('click', '#tablaPedidos button', function(){
    event.stopPropagation();
    event.stopImmediatePropagation();
    pedido=document.getElementById(this.id).dataset.id;
    cadena=this.id;

    if(cadena.includes('btnVer')){
      cargarPlatillos();
    }

    if(cadena.includes('btnCambiarEstado')){
      cambiarEstado();
    }

  });

  $("#btnActualizarTablaPedidos").click(function(){
    filtrarPedidos();

  });

  $(document).on('change','#estadoPedido',function(){ seleccionarEstado() })

  function seleccionarEstado(){

      estado=  document.getElementById('estadoPedido').value;

      //Toast2.fire({ icon:'error', title: estado})
  };
  function filtrarPedidos(){
    if(sucursal==0){
        //Toast2.fire({ icon:'error', title: "seleccione una sucursal!"})
    }else{
      $.ajax({
          url: "/pedido/filtrar_pedidos_ajax",
          data: {
             'id': sucursal,
          },
          type: 'POST',
          error: function(err) {
              //console.error(err);
          },
          success: function(data) {
            ////alert("filtrados");
            $("#tablaPedidos").html("");
            $("#tablaPedidos").html(data);
          },
          complete: function() {}
      })
    }
  };
  function cambiarEstado(){
    if(estado==0){
        //Toast2.fire({ icon:'error', title: "seleccione un estado!"})
    }else{
      $.ajax({
          url: "/pedido/cambiar_estado_ajax",
          data: {
             'pedido': pedido,
             'estado': estado,
          },
          type: 'POST',
          error: function(err) {
              //console.error(err);
          },
          success: function(data) {
            //Toast2.fire({ icon:'error', title: "cambiado!"})

          },
          complete: function() {}
      })
    }
  };
  function cargarPlatillos(){
    $.ajax({
        url: "/pedido/cargar_platillos_ajax",
        data: {
           'pedido': pedido,
        },
        type: 'POST',
        error: function(err) {
            //console.error(err);
        },
        success: function(data) {
          $("#tablaProductos").html("");
          $("#tablaProductos").append(data);
        },
        complete: function() {}
    })
  };


});
