
  var DATA_COUNT = 5;
  var utils = Samples.utils;
  utils.srand(110);
  var labels =["uno", "dos", "tres", "cuatro", "cinco", "seis"];// arreglo de titulos
  var datos= [10,20,30,40,50,60]; //arreglo de datos
  var ids= [10,11,12,13,14,15]; //arreglo de ids
  var authElements;
          //----------COLORES A USAR
          var colores=
            ["rgba(255, 99, 132, 0.6)",	"rgba(255, 159, 64, 0.6)",	"rgba(255, 205, 86, 0.6)",	"rgba(75, 192, 192, 0.6)","rgba(54, 162, 235, 0.6)",	"rgba(153, 102, 255, 0.6)",	"rgba(201, 203, 207, 0.6)", "rgba(158,143,80,0.2)", "rgba(52,123,111,0.2)", "rgba(241,34,133,0.2)", "rgba(17,73,133,0.2)"]
      var colorTransparentes=
            ["rgb(255, 99, 132)",	"rgb(255, 159, 64)",	"rgb(255, 205, 86)",	"rgb(75, 192, 192)",	"rgb(54, 162, 235)",	"rgb(153, 102, 255)","rgb(201, 203, 207)","RGB(158,143,80)", "RGB(52,123,111)", "RGB(241,34,133)", "RGB(17,73,133)"]
  var p1= document.getElementById('contenidoMensajes');var p2= document.getElementById('contenidoPedidos');var p3= document.getElementById('contenidoPlatillosProductos');var p4= document.getElementById('contenidoSucursales');
  var p5= document.getElementById('contenidoGraficosAdmin');
  if(p1 !=null){
    p1.style.display = 'none';
  }
  if(p2 !=null){
    p2.style.display = 'none';
  }
  if(p3 !=null){
    p3.style.display = 'none';
  }
  if(p4 !=null){
    p4.style.display = 'none';
  }
  if(p5 !=null){
    p5.style.display = 'block';
  }
  var p6= document.getElementById('contenidoReportes');
  if(p6 !=null ){
      p6.style.display = 'none';
  }

  
$('#downloadPdf').click(function(event) {
    // get size of report page
    var reportPageHeight = $('#chart-productos').innerHeight();
    var reportPageWidth =  $('#chart-productos').innerWidth();
    
    // create a new canvas object that we will populate with all other canvas objects
    var pdfCanvas = $('<canvas />').attr({
      id: "canvaspdf",
      width: reportPageWidth,
      height: reportPageHeight
    });
    
    // keep track canvas position
    var pdfctx = $(pdfCanvas)[0].getContext('2d');
    var pdfctxX = 0;
    var pdfctxY = 0;
    var buffer = 100;
    
    // for each chart.js chart
    $("canvas").each(function(index) {
      // get the chart height/width
      var canvasHeight = $(this).innerHeight();
      var canvasWidth = $(this).innerWidth();
      
      // draw the chart into the new canvas
      pdfctx.drawImage($(this)[0], pdfctxX, pdfctxY, canvasWidth, canvasHeight);
      pdfctxX += canvasWidth + buffer;
      
      // our report page is in a grid pattern so replicate that in the new canvas
      if (index % 2 === 1) {
        pdfctxX = 0;
        pdfctxY += canvasHeight + buffer;
      }
    });
    // create new pdf and add our new canvas as an image
    var pdf = new jsPDF('l', 'pt', [reportPageWidth, reportPageHeight]);
    pdf.addImage($(pdfCanvas)[0], 'PNG', 0, 0);
    
    // download the pdf
    pdf.save('filename.pdf');
  });

  function getToken(){
    var token;
    $.ajax({
        url:"/socketval/pedir_token",
        async: false,
        type:'POST',
        data: {
        },
        error:function(err){
              console.error(err);
        },
        success:function(data){
            var st= Object.values(data);
            token= data.data;
        },
        complete:function(){
        }
    });
    return token;
  }

  var  notifEstadisticas = new WebSocket("ws://localhost:8080");
  function NotifServer(){
     notifEstadisticas.onmessage = function (event) {

      }
      notifEstadisticas.onopen = function() {
        authElements ="{\"tipo\":\"4\",\"sucursal\":\"1\",\"token\":\""+getToken()+"\",\"userData\":\""+idUsuario+"\" }";
        notifEstadisticas.send(authElements);
      }
      notifEstadisticas.onerror = function(error) {
        //alert('error');
      }
  }
    NotifServer();

  function MensajeEstadistico(){
    authElements =  "{\"tipo\":\"4\",\"sucursal\":\"1\",\"producto\":\"1\",\"cantidad\":\"10\" ,\"token\":\""+getToken()+"\",\"userData\":\""+idUsuario+"\"}";
    notifEstadisticas.send(authElements);
  }

  function getRandomArbitrary(x,y) {
    return Math.floor(Math.random()*(y-x))+x
  }
  var optionsBarra = { //Configuración inicial del grafico
      maintainAspectRatio: false,
      responsive: true,
      scales: {
          xAxes: [{ stacked: true }],
      },
      legend: false,
      tooltips: false,
      elements: {
        rectangle: {
          backgroundColor: colores,
          borderColor: colorTransparentes,
          borderWidth: 1
        }
      },
      title: {
          display: true,
          text: 'Número de productos vendidos'
        }
   };
   var dataBarra = { //Datos iniciales del grafico
       labels: labels, ids: ids,
       datasets: [{
         data: datos
   }]};
  var graficoBarraProductos = new Chart( //Se crean el grafico de barras
    document.getElementById("chart-productos"),{
      "type":"horizontalBar",
        "data": dataBarra,
          "options": optionsBarra
    });
//--------ejemplo de modificaciones directas al grafico NO USADAS, AMBOS GRAFICOS SE HAN ENLAZADO POR VECTORES DE DATOS
/*		function agregarBarraProducto(id, producto, cantidad) {
     var color=getRandomArbitrary(0,10);
     graficoBarraProductos.data.labels.push(producto);
     graficoBarraProductos.data.ids.push(id);
     graficoBarraProductos.data.datasets[0].data.push(cantidad); //Valor a graficar
     graficoBarraProductos.options.elements.rectangle.backgroundColor.push(graficoBarraProductos.options.elements.rectangle.backgroundColor[color]);
     graficoBarraProductos.options.elements.rectangle.borderColor.push(graficoBarraProductos.options.elements.rectangle.borderColor[color]);
     actualizarGraficos();
     var alto= document.getElementById('chart-productos').offsetHeight;
     alto=alto+40;
     graficoBarraProductos.canvas.parentNode.style.height = alto+'px';
     graficoBarraProductos.resize();
}
function removerBarraProducto(producto) {
  var index = graficoBarraProductos.data.ids.indexOf(producto);
  graficoBarraProductos.data.ids.splice(index, 1);
  graficoBarraProductos.data.datasets[0].data.splice(index, 1);
  graficoBarraProductos.data.labels.splice(index, 1);
  graficoBarraProductos.options.elements.rectangle.backgroundColor.splice(index, 1);
  graficoBarraProductos.options.elements.rectangle.borderColor.splice(index, 1);
  actualizarGraficos();
}

function cambiarDato(producto, cantidad) {
   var index = graficoBarraProductos.data.ids.indexOf(producto);//Bucar producto en los id
   var cantidadAnterior= graficoBarraProductos.data.datasets[0].data[index]; // extraer el valor de productos
   graficoBarraProductos.data.datasets[0].data[index]=cantidadAnterior+cantidad; // cambiar el valor por la suma de las cantidades
   actualizarGraficos();
}*/
//----------------------------------------------------------------------------

function generateData() {
return utils.numbers({
  count: DATA_COUNT,
  min: 0,
  max: 100
});
}
var data = {
datasets: [{	data: datos }],
labels: labels, ids: ids,
};

var options = {
  elements: {
    arc: {
      backgroundColor:colores,
      hoverBackgroundColor: colorTransparentes,
      borderWidth: 1
    }
  },
title: {
    display: true,
    text: 'Número de productos vendidos'
  }
};
var graficoCircularProductos = new Chart('chart-graficoCircular', { // crea grafico en chart-1
  type: 'pie',
  data: data, // toma los datos
  options: options
});
//------------MODIFICACIONES AL GRAFICO-------------
function AgregarProducto(id,  producto, cantidad) {
 var color=getRandomArbitrary(0,10);
 labels.push(producto); //Titulo en la barra
 ids.push(id);// GUARDAR EL ID DEL PRODUCTO para reconocerlo en el array del grafico
 datos.push(cantidad); //Valor a graficar
 colores.push(colores[color]);
 colorTransparentes.push(colorTransparentes[color]);
 actualizarGraficos();
 graficoCircularProductos.resize();
}
function removerProducto(producto) {
var index = graficoBarraProductos.data.ids.indexOf(producto);
ids.splice(index, 1);
datos.splice(index, 1);
labels.splice(index, 1);
colores.splice(index, 1);
colorTransparentes.splice(index, 1);
actualizarGraficos();
}
function cambiarDato(producto, cantidad) {
 var index = ids.indexOf(producto);//Bucar producto en los id
 var cantidadAnterior= datos[index]; // extraer el valor de productos
 datos[index]=cantidadAnterior+cantidad; // cambiar el valor por la suma de las cantidades
 actualizarGraficos();
}
//-------------------------------------------------
//---------------RENDERIZAR CAMBIOS----------------
function actualizarGraficos(){
graficoCircularProductos.update();//NOTA: El cambio en un grafico automaticamente cambia a los dos
graficoBarraProductos.update();
}
