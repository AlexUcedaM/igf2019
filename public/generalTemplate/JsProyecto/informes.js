$(document).ready(function() {


    var p1= document.getElementById('contenidoMensajes');
    if(p1 !=null){
        p1.style.display = 'none';
    }
    var p2= document.getElementById('contenidoPedidos');
    if(p2 !=null){
        p2.style.display = 'none';
    }
    var p3= document.getElementById('contenidoPlatillosProductos');
    if(p3 !=null){
        p3.style.display = 'none';
    }
    var p4= document.getElementById('contenidoSucursales');
    if(p4 !=null){
        p4.style.display = 'none';
    }
    var p5= document.getElementById('contenidoGraficosAdmin');
    if(p5 !=null){
        p5.style.display = 'none';
    }
    var p6= document.getElementById('contenidoReportes');
    if(p6 !=null ){
        p6.style.display = 'block';
    }

    $("#generar").click(function() {
        document.getElementById('tabla').innerHTML='';
        generarInforme();
        generarInformePDF();
    });

    //--------------------------------------------------------
    
function generarInformePDF(){
    location.href='/reportePDF/'+sucursal+"/"+$("#fechaInicial").val()+"/"+$("#fechaFinal").val()+"/"
  };
  
  function generarInforme(){
  var groupColumn = 0;
    $.ajax({
        url: "/reporteEstadisticas",
        type: 'POST',
        data: {
          'fechaInicial': $("#fechaInicial").val(),
          'fechaFinal': $("#fechaFinal").val(),
          'sucursal' : sucursal
        },
        success: function (data) {
          
          $("#tablaImpresion").html(data);
          $("#tabla").html(data);
  
          
  
        },
        fail: function () {
            alert("Error");
        },
        complete: function () {
  
          var printCounter = 0;
        
          $('#tablaIncrustada1').DataTable({"lengthMenu": [[ -1, 100,50,15], [ "All",100,50,15]],responsive: true,"ordering" : true,dom: 'Bfrtip',
              buttons: ['copyHtml5', 'excelHtml5', 'pdfHtml5', 'csvHtml5', 'print',  'colvis'],
              "order": [[ groupColumn, 'asc' ]],
              "drawCallback": function ( settings ) {var api = this.api();var rows = api.rows( {page:'current'} ).nodes();var last=null;
                  api.column(groupColumn, {page:'current'} ).data().each( function ( group, i ) {
                      if ( last !== group ) {
                          $(rows).eq( i ).before(
                              '<tr class="group"><td colspan="5">'+group+'</td></tr>'
                          );
                          last = group;
          }});},"scrollX": true,"font-size": "5px",})
          $('#tablaIncrustada2').DataTable({"lengthMenu": [[ -1, 100,50,15], [ "All",100,50,15]],responsive: true,"ordering" : true,dom: 'Bfrtip',
              buttons: ['copyHtml5', 'excelHtml5', 'pdfHtml5', 'csvHtml5', 'print',  'colvis'],
              "order": [[ groupColumn, 'asc' ]],
              "drawCallback": function ( settings ) {var api = this.api();var rows = api.rows( {page:'current'} ).nodes();var last=null;
                  api.column(groupColumn, {page:'current'} ).data().each( function ( group, i ) {
                      if ( last !== group ) {
                          $(rows).eq( i ).before(
                              '<tr class="group"><td colspan="5">'+group+'</td></tr>'
                          );
                          last = group;
          }});},"scrollX": true,"font-size": "5px",})
          $('#tablaIncrustada3').DataTable({"lengthMenu": [[ -1, 100,50,15], [ "All",100,50,15]],responsive: true,"ordering" : true,dom: 'Bfrtip',
              buttons: ['copyHtml5', 'excelHtml5', 'pdfHtml5', 'csvHtml5', 'print',  'colvis'],
              "order": [[ groupColumn, 'asc' ]],
              "drawCallback": function ( settings ) {var api = this.api();var rows = api.rows( {page:'current'} ).nodes();var last=null;
                  api.column(groupColumn, {page:'current'} ).data().each( function ( group, i ) {
                      if ( last !== group ) {
                          $(rows).eq( i ).before(
                              '<tr class="group"><td colspan="5">'+group+'</td></tr>'
                          );
                          last = group;
          }});},"scrollX": true,"font-size": "5px",})
          $('#tablaIncrustada4').DataTable({"lengthMenu": [[ -1, 100,50,15], [ "All",100,50,15]],responsive: true,"ordering" : true,dom: 'Bfrtip',
              buttons: ['copyHtml5', 'excelHtml5', 'pdfHtml5', 'csvHtml5', 'print',  'colvis'],
              "order": [[ groupColumn, 'asc' ]],
              "drawCallback": function ( settings ) {var api = this.api();var rows = api.rows( {page:'current'} ).nodes();var last=null;
                  api.column(groupColumn, {page:'current'} ).data().each( function ( group, i ) {
                      if ( last !== group ) {
                          $(rows).eq( i ).before(
                              '<tr class="group"><td colspan="5">'+group+'</td></tr>'
                          );
                          last = group;
          }});},"scrollX": true,"font-size": "5px",})
          $('#tablaIncrustada5').DataTable({"lengthMenu": [[ -1, 100,50,15], [ "All",100,50,15]],responsive: true,"ordering" : true,dom: 'Bfrtip',
              buttons: ['copyHtml5', 'excelHtml5', 'pdfHtml5', 'csvHtml5', 'print',  'colvis'],
              "order": [[ groupColumn, 'asc' ]],
              "drawCallback": function ( settings ) {var api = this.api();var rows = api.rows( {page:'current'} ).nodes();var last=null;
                  api.column(groupColumn, {page:'current'} ).data().each( function ( group, i ) {
                      if ( last !== group ) {
                          $(rows).eq( i ).before(
                              '<tr class="group"><td colspan="5">'+group+'</td></tr>'
                          );
                          last = group;
          }});},"scrollX": true,"font-size": "5px",})
          $('#tablaIncrustada6').DataTable({"lengthMenu": [[ -1, 100,50,15], [ "All",100,50,15]],responsive: true,"ordering" : true,dom: 'Bfrtip',
              buttons: ['copyHtml5', 'excelHtml5', 'pdfHtml5', 'csvHtml5', 'print',  'colvis'],
              "order": [[ groupColumn, 'asc' ]],
              "drawCallback": function ( settings ) {var api = this.api();var rows = api.rows( {page:'current'} ).nodes();var last=null;
                  api.column(groupColumn, {page:'current'} ).data().each( function ( group, i ) {
                      if ( last !== group ) {
                          $(rows).eq( i ).before(
                              '<tr class="group"><td colspan="5">'+group+'</td></tr>'
                          );
                          last = group;
          }});},"scrollX": true,"font-size": "5px",})
          $('#tablaIncrustada7').DataTable({"lengthMenu": [[ -1, 100,50,15], [ "All",100,50,15]],responsive: true,"ordering" : true,dom: 'Bfrtip',
              buttons: ['copyHtml5', 'excelHtml5', 'pdfHtml5', 'csvHtml5', 'print',  'colvis'],
              "order": [[ groupColumn, 'asc' ]],
              "drawCallback": function ( settings ) {var api = this.api();var rows = api.rows( {page:'current'} ).nodes();var last=null;
                  api.column(groupColumn, {page:'current'} ).data().each( function ( group, i ) {
                      if ( last !== group ) {
                          $(rows).eq( i ).before(
                              '<tr class="group"><td colspan="5">'+group+'</td></tr>'
                          );
                          last = group;
          }});},"scrollX": true,"font-size": "5px",})
          $('#tablaIncrustada8').DataTable({"lengthMenu": [[ -1, 100,50,15], [ "All",100,50,15]],responsive: true,"ordering" : true,dom: 'Bfrtip',
              buttons: ['copyHtml5', 'excelHtml5', 'pdfHtml5', 'csvHtml5', 'print',  'colvis'],
              "order": [[ groupColumn, 'asc' ]],
              "drawCallback": function ( settings ) {var api = this.api();var rows = api.rows( {page:'current'} ).nodes();var last=null;
                  api.column(groupColumn, {page:'current'} ).data().each( function ( group, i ) {
                      if ( last !== group ) {
                          $(rows).eq( i ).before(
                              '<tr class="group"><td colspan="5">'+group+'</td></tr>'
                          );
                          last = group;
          }});},"scrollX": true,"font-size": "5px",})
        }}
  )}



});


